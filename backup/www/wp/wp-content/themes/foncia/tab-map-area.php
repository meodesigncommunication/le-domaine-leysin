<?php
$latitude = get_post_meta($post->ID, "latitude", true);
$longitude = get_post_meta($post->ID, "longitude", true);
?>
<div class="map-area tab">
	<div id="map-area">
		<?php if (!empty($latitude) and !empty($longitude)) { ?>
			<script>
			var latitude = <?php echo $latitude; ?>,
				longitude = <?php echo $longitude; ?>;
			</script>
		<?php } ?>
		<div class="map-wrapper">
			<div id="map">&nbsp;</div>
		</div>
		<div class="clear"></div>
	</div>
</div>