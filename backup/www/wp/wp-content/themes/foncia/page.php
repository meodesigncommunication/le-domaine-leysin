<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>
<?php $on = get_post_meta($post->ID, "slider_on", true); ?>
<?php if($on) : ?>

	<div class="<?php echo is_front_page() ? 'slideshow-container-home' : 'slideshow-container'; ?>"><?php
		if (is_front_page() and (time() < strtotime("2013-07-21")) ) {
			$file = "splash." . qtrans_getLanguage() . ".png";
			if (file_exists(get_stylesheet_directory() . '/images/' . $file)) {
				?><img class="splash" src="<?php echo get_stylesheet_directory_uri(); ?>/images/<?php echo $file; ?>" alt=""/><?php
			}
		}


		$fieldset_data = get_cft_fieldset_data('slider_' . qtrans_getLanguage() . '-');
		$fieldset_data = order_array_num($fieldset_data, 'slider_' . qtrans_getLanguage() . '-banner_order', 'ASC');
		if($fieldset_data) : ?>

			<?php echo '<ul class="slideshow">'; ?>
			<?php foreach($fieldset_data as $k => $item) :
				if(!$item['slider_' . qtrans_getLanguage() . '-banner_image']) continue; ?>
				<?php $slider_img = $item['slider_' . qtrans_getLanguage() . '-banner_image']; ?>

				<?php echo '<li>'; ?>
				<div class="item">
						<?php 
						echo wp_get_attachment_image($slider_img, 'full');
						?>	
				</div>
				<?php echo '</li>'; ?>
			<?php endforeach; ?>
			<?php echo '</ul>'; ?>

		<?php endif; ?>
		<?php 
	
	// Admin only for now
	//if ( is_user_logged_in() && current_user_can('activate_plugins') ) { 
		
		// GET Fixed Post ID to fill text content and/or featured image ass background
		$postIDtoPrint = 753;
		$postToPrint = get_post( $postIDtoPrint ); // 753 is hardcoded
		
		if($postToPrint && is_object($postToPrint)){
			
			$bgDiv = ''; // <div class="slider-text-img"></div>
			// Featured Image
			if ( has_post_thumbnail($postIDtoPrint) ) {
				// $bgDiv = '<div class="slider-text-img" style="background:url(\''.wp_get_attachment_url( get_post_thumbnail_id($postIDtoPrint) ).'\') 0 0 no-repeat;"></div>';
			}
			
			echo '<div class="slider-text-container">
					<div class="slider-text-sub-container">
						<div class="slider-text-left">
							<div class="slider-text-content">'.__($postToPrint->post_content).'</div>'.$bgDiv.'
						</div>
						<div class="slider-text-right"></div>
					</div>
				  </div>';
			
		}
		
		
		wp_reset_postdata();		
	//}
	
	?>
	</div><!-- end slider-container-->
<?php endif; ?>
		

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'page' ); ?>
					
				<?php endwhile; // end of the loop. ?>
	
			

<?php get_footer(); ?>