<?php
/**
 * Template Name: Form 1
 *
 * @package ledomainleysin
 */
?>

<html>
    <head>
      <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); echo '?' . filemtime( get_stylesheet_directory() . '/style.css'); ?>" />
    </head>
    
    
    <?php
      $form_labels = array(
	'form_title' => __('To receive your download link,<br>please provide the following information:', LEYSIN_TEXT_DOMAIN),

	'surname'    => __('Surname', LEYSIN_TEXT_DOMAIN),
	'first_name' => __('First name', LEYSIN_TEXT_DOMAIN),
	'email'      => __('email', LEYSIN_TEXT_DOMAIN),
	'phone'      => __('Tel', LEYSIN_TEXT_DOMAIN),
	'address'    => __('Address', LEYSIN_TEXT_DOMAIN),
	'postcode'   => __('Postcode', LEYSIN_TEXT_DOMAIN),
	'city'       => __('City', LEYSIN_TEXT_DOMAIN),

	'required'   => __('required fields', LEYSIN_TEXT_DOMAIN),

	'send'       => __('send', LEYSIN_TEXT_DOMAIN),

	'sent'       => __('The download link for your PDF has been sent to your email address', LEYSIN_TEXT_DOMAIN),
	'ok'         => __('OK', LEYSIN_TEXT_DOMAIN),

	'send_to'    => __('The download link for your PDF will be sent to your email address', LEYSIN_TEXT_DOMAIN),

	'change_address' => __("Change", LEYSIN_TEXT_DOMAIN)
      );
    ?>
  <body>
    <div id="pdf_download">
      <div id="pdf_download_form" class="download-form">
        <form id="customer_registration_form" action="<?php echo get_permalink( get_page_by_path( 'form-handler' ) ); ?>" method="post">
          <input id="pdf_apartment_code" type="hidden" name="apartment_code" value="">
          <div class="initial">
            <div class="form_row">
              <div class="pdf_icon">&nbsp;</div><div class="popup_heading"><?php echo $form_labels['form_title']; ?></div>
            </div>
            <div class="form_row">
              <input                   type="text" class="surname"    name="surname"    placeholder="<?php echo $form_labels['surname'];    ?>*">
              <div class="input_state" id="surname_state"></div>
            </div>
            <div class="form_row">
              <input                   type="text" class="first_name" name="first_name" placeholder="<?php echo $form_labels['first_name']; ?>*">
              <div class="input_state" id="first_name_state"></div>
            </div>
            <div class="form_row">
              <input                   type="text" class="email"      name="email"      placeholder="<?php echo $form_labels['email'];      ?>*">
              <div class="input_state" id="email_state"></div>
            </div>
            <div class="form_row">
              <input                   type="text" class="phone"      name="phone"      placeholder="<?php echo $form_labels['phone'];      ?>*">
              <div class="input_state" id="phone_state"></div>
            </div>
            <div class="form_row">
              <input                   type="text" class="address"    name="address"    placeholder="<?php echo $form_labels['address'];    ?>*">
              <div class="input_state" id="address_state"></div>
            </div>
            <div class="form_row">
              <input id="pdf_postcode" type="text" class="postcode"   name="postcode"   placeholder="<?php echo $form_labels['postcode'];   ?>*">
              <div class="input_state" id="postcode_state"></div>
            </div>
            <div class="form_row">
              <input id="pdf_city"     type="text" class="city"       name="city"       placeholder="<?php echo $form_labels['city'];       ?>*">
              <div class="input_state" id="city_state"></div>
            </div>
            <div class="form_row">
              <?php echo foncia_get_country_select(); ?>
              <div class="input_state valid" style="visibility: hidden;"></div>
            </div>
            <div class="form_row form_row_required">
              <div class="caption"> * <?php echo $form_labels['required']; ?></div>
              <input type="hidden" name="language" value="<?php echo qtrans_getLanguage(); ?>">
            </div>
            <div class="form_row form_row_buttons">
              <button type="submit" disabled="disabled"><?php echo $form_labels['send']; ?></button>
            </div>
          </div>
          <div class="subsequent" style="display: none;">
            <div class="form_row">
              <div class="pdf_icon">&nbsp;</div><div class="popup_heading"><?php echo $form_labels['send_to']; ?></div>
            </div>
            <div class="form_row">
              <div class="pdf_email_address">&nbsp;</div>
            </div>
            <div class="form_row form_row_buttons">
              <button class="change"><?php echo $form_labels['change_address']; ?></button>
              <button type="submit" disabled="disabled"><?php echo $form_labels['ok']; ?></button>
            </div>
          </div>
        </form>
      </div>
      <div id="pdf_download_thanks" class="download-form" style="display: none;">
        <div class="form_row">
          <div class="pdf_icon">&nbsp;</div><div class="popup_heading"><?php echo $form_labels['sent']; ?></div>
        </div>
        <div class="pdf_email_address">&nbsp;</div>
        <div class="form_row form_row_buttons">
          <button type="submit"><?php echo $form_labels['ok']; ?></button>
        </div>
      </div>
    </div>
  </body>
</html>