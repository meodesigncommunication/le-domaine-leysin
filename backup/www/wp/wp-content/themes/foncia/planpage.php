<?php
/*
Template Name: plan page
*/

get_header(); ?>
		

				<?php while (have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'planpage' ); ?>
					
				<?php endwhile; // end of the loop. ?>
	
			

<?php get_footer(); ?>