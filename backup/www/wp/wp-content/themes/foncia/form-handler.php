<?php
/**
 * Template Name: Form handler
 *
 */

add_action( 'phpmailer_init', 'foncia_phpmailer_init' );
function foncia_phpmailer_init( PHPMailer $phpmailer ) {
    $phpmailer->Host = 'mail.infomaniak.com';
    $phpmailer->Port = 587;
    $phpmailer->Username = 'info@ledomaineleysin.ch';
    $phpmailer->Password = '4wWZUTaVGxKu';
    $phpmailer->SMTPAuth = true;
    $phpmailer->IsSMTP();
}

define('IN_DEBUG_MODE', (stripos($_SERVER['HTTP_HOST'], 'ledomaineleysin.ch') === false) );
define('MEO_EMAIL', 'smartcapture.meo@gmail.com');
define('PE_EMAIL', 'lmanzoni@pierreetoile.ch');
define('ADMIN_EMAIL', IN_DEBUG_MODE ? MEO_EMAIL : PE_EMAIL);
define('SERVER', 'ledomaineleysin.ch');
define('SITE_DESCRIPTION_SHORT', 'Le Domaine Du Parc');
define('SITE_DESCRIPTION_FULL', SITE_DESCRIPTION_SHORT . ', Leysin');

function foncia_send_new_contact_email($recipient, $surname, $first_name, $email, $phone, $address, $postcode, $city, $country, $language, $subject, $apartment_code, $error = false) {
	$headers = array(
		'From: "' . $surname . ', ' . $first_name . '" <' . $email . '>',
		'Content-type: text/html; charset="utf-8"',
		'Bcc: ' . MEO_EMAIL
	);

	$downloads_url = get_permalink( get_page_by_path( 'downloads' ) );

	$content = "Nom: $surname<br>" .
			   "Prénom: $first_name<br>" .
			   "email: $email<br>" .
			   "Télephone: $phone<br>" .
			   "Adresse: $address<br>" .
			   "NPA: $postcode<br>" .
			   "Lieu: $city<br>" .
			   ( empty($country) ? '' : "Pays: $country->name<br>" ) .
			   "Langue: $language<br><br>" .
			   "Appartement: $apartment_code<br><br>" .
			   'Voir la liste: <a href="' . $downloads_url . '">' . $downloads_url . "</a>";

	$success = wp_mail( $recipient, SITE_DESCRIPTION_SHORT . ' :: ' . $subject, $content, $headers );

	if (!$success and !$error) {
		foncia_send_new_contact_email(PE_EMAIL, $surname, $first_name, $email, $phone, $address, $postcode, $city, $country, $language, 'Error emailing pierre etoile', $apartment_code, true);
	}
}




function foncia_get_pdf_for_apartment($apartment_code) {
	global $wpdb;

	$sql = $wpdb->prepare(
		"select post_id
		   from $wpdb->postmeta apt
		  where apt.meta_key like 'floor-code%%'
		    and apt.meta_value = %s",
		$apartment_code
	);

	$building_post_id = $wpdb->get_var( $sql );

	$floor_details = get_cft_fieldset_data('floor-', $building_post_id);

	foreach ($floor_details as $floor_detail) {
		for ($apartment = 1; $apartment <= 4; $apartment++) {
			if ($floor_detail['floor-code' . $apartment] == $apartment_code) {
				return $floor_detail['floor-plan-pdf' . $apartment];
			}
		}
	}

	return null;
}

function foncia_get_downloader_id($surname, $first_name, $email, $phone, $address, $postcode, $city, $language, $country_code, $apartment_code) {
	global $wpdb;

	$country = foncia_get_country_by_code($country_code);

	$sql = $wpdb->prepare(
		"select pd.id
		   from pdf_downloader pd
		  where pd.email = %s",
		$email
	);

	$downloader_id = $wpdb->get_var( $sql );

	if (empty($downloader_id)) {
		foncia_send_new_contact_email(PE_EMAIL, $surname, $first_name, $email, $phone, $address, $postcode, $city, $country, $language, 'PDF commandé', $apartment_code);

		$values = array(
			'surname'       => $surname,
			'first_name'    => $first_name,
			'email'         => $email,
			'phone'         => $phone,
			'address'       => $address,
			'postcode'      => $postcode,
			'city'          => $city,
			'language'      => $language,
			'creation_date' => current_time('mysql', 1)
		);

		$placeholders = array(
			'%s', // surname
			'%s', // first_name
			'%s', // email
			'%s', // phone
			'%s', // address
			'%s', // postcode
			'%s', // city
			'%s', // language
			'%s'  // creation_date
		);

		if (!empty($country) and !empty($country->country_id)) {
			$values['country_id'] = $country->country_id;
			$placeholders[] = '%d';
		}

		$wpdb->insert(
			'pdf_downloader',
			$values,
			$placeholders
		);

		$downloader_id = $wpdb->insert_id;
	}

	return $downloader_id;
}


function foncia_get_download_code($downloader_id, $pdf_post_id) {
	global $wpdb;
	$download_code = $wpdb->get_var('SELECT UUID_SHORT() as download_code');

	$result = $wpdb->insert
	(
		'pdf_download',
		array(
			'pdf_downloader_id' => $downloader_id,
			'pdf_post_id'       => $pdf_post_id,
			'download_code'     => $download_code
		),
		array(
			'%d',
			'%d',
			'%s'
		)
	);

	if (false === $result) {
		return null;
	}

	return $download_code;
}

function foncia_send_download_link_to_user($email, $download_code, $pdf_post_id, $apartment_code) {
	require_once ABSPATH . WPINC . '/class-phpmailer.php';
	require_once ABSPATH . WPINC . '/class-smtp.php';
	$phpmailer = new PHPMailer( true );

	$phpmailer->CharSet = 'UTF-8';

	$phpmailer->Host = 'mail.infomaniak.com';
	$phpmailer->Port = 587;
	$phpmailer->Username = 'info@coeurdevevey.ch';
	$phpmailer->Password = 'aiglesurlalune';
	$phpmailer->SMTPAuth = true;

	$phpmailer->IsSMTP();

	$admin_email = PE_EMAIL;

	$phpmailer->ContentType = "multipart/related";
	$phpmailer->SetFrom($admin_email, SITE_DESCRIPTION_SHORT);
	$phpmailer->AddAddress($email);

	$pierre_etoile_logo = get_stylesheet_directory() . "/images/pe_logo_email.jpg";
	$cid = "pierre-etoile-logo";
	$phpmailer->AddEmbeddedImage($pierre_etoile_logo, $cid);

	$phpmailer->Subject    = SITE_DESCRIPTION_SHORT . ' - ' . __( 'apartment ', LEYSIN_TEXT_DOMAIN ) . $apartment_code;

	$download_url = get_permalink( get_page_by_path( 'pdf-download' ) ) . '?email=' . urlencode($email) . '&download_code=' . urlencode($download_code);


	$content = "<html>\n";
	$content .= "<body style=\"word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space; \">\n";
	$content .= __('Hello', LEYSIN_TEXT_DOMAIN) . ",\n";

	$content .= "<div><br></div>\n";
	$content .= "<div>" . __('Thank you for your interest in the ', LEYSIN_TEXT_DOMAIN) .
	                    SITE_DESCRIPTION_FULL .
	                    __(' project.', LEYSIN_TEXT_DOMAIN) .
	            "</div>\n";

	$content .= "<div><br></div>\n";
	$content .= "<div>" .
					__('Below is the link to download the plan for apartment ', LEYSIN_TEXT_DOMAIN) .
					$apartment_code .
					__(' as a PDF:', LEYSIN_TEXT_DOMAIN) .
				"</div>\n";

	$content .= "<div><a href=\"" . $download_url . "\">" . $download_url . "</a></div>\n";
	$content .= "<div><br></div>\n";
	$content .= "<div><br></div>\n";
	$content .= "<div>" . __("Kind regards,", LEYSIN_TEXT_DOMAIN) . "</div>\n";
	$content .= "<div><br></div>\n";
	$content .= "<div>" . SITE_DESCRIPTION_FULL . ' ' . __("program manager", LEYSIN_TEXT_DOMAIN) . "</div>\n";
	$content .= "<div>Laurent MANZONI</div>\n";
    $content .= "<div><br></div>\n";

    // Footer
    $content .= "<div><br></div>\n";
    $content .= '<div><font color="#7c7e7b">---------------------------------------</font></div>';
    $content .= "<div><br></div>\n";
    $content .= "<div><br></div>\n";
    $content .= '<div><img src="cid:' . $cid . '" alt="Pierre Etoile" width="193" height="45"></div>';
    $content .= "<div><br></div>\n";
    $content .= "<div><br></div>\n";
    $content .= '<div><font color="#7c7e7b">' . __("Sales office", LEYSIN_TEXT_DOMAIN) . '</font></div>';
    $content .= '<div><font color="#7c7e7b">Chemin du Flonzel 59</font></div>';
    $content .= '<div><font color="#7c7e7b">1093 La Conversion</font></div>';
    $content .= "<div><br></div>\n";
    $content .= '<div><font color="#7c7e7b">+41 (0) 21 791 18 19</font></div>';
    $content .= '<div><font color="#7c7e7b">+41 (0) 79 108 89 58</font></div>';
    $content .= '<div><a href="mailto:' . PE_EMAIL . '"><font color="#7c7e7b">' . PE_EMAIL . '</font></a></div>';
    $content .= "<div><br></div>\n";
    $content .= "<div><br></div>\n";
    $content .= '<div><font color="#7c7e7b">---------------------------------------</font></div>';
    $content .= '</body>';
    $content .= '</html>';

    $content = foncia_escape_html($content);

    $phpmailer->MsgHTML($content);

    $result = $phpmailer->Send();

    return $result;
}


$result_message = '';
$success = false;

if (empty($_POST)) {
	$result_message = __('Please fill in the form', LEYSIN_TEXT_DOMAIN);
}
else {
	$apartment_code = stripslashes($_POST['apartment_code']);
	$surname        = stripslashes($_POST['surname']);
	$first_name     = stripslashes($_POST['first_name']);
	$email          = stripslashes($_POST['email']);
	$phone          = stripslashes($_POST['phone']);
	$address        = stripslashes($_POST['address']);
	$postcode       = stripslashes($_POST['postcode']);
	$city           = stripslashes($_POST['city']);
	$language       = stripslashes($_POST['language']);
	$country_code   = stripslashes($_POST['country']);

	$apartment_code = empty($apartment_code) ? '' : $apartment_code;
	$surname        = empty($surname)        ? '' : $surname;
	$first_name     = empty($first_name)     ? '' : $first_name;
	$email          = empty($email)          ? '' : $email;
	$phone          = empty($phone)          ? '' : $phone;
	$address        = empty($address)        ? '' : $address;
	$postcode       = empty($postcode)       ? '' : $postcode;
	$city           = empty($city)           ? '' : $city;
	$language       = empty($language)       ? '' : $language;
	$country_code   = empty($country_code)   ? '' : $country_code;

	$pdf_post_id = null;

	if (!is_email($email)) {
		$result_message = __("Please provide a valid email address", LEYSIN_TEXT_DOMAIN);
	}
	else {
		$pdf_post_id = foncia_get_pdf_for_apartment($apartment_code);
		if (empty($pdf_post_id)) {
			$result_message = __("Invalid apartment code", LEYSIN_TEXT_DOMAIN);
		}
	}

	if (!$result_message) {
		$pdf_downloader_id = foncia_get_downloader_id($surname, $first_name, $email, $phone, $address, $postcode, $city, $language, $country_code, $apartment_code);
		if (empty($pdf_downloader_id)) {
			$result_message = __("An error occurred sending your message.", LEYSIN_TEXT_DOMAIN);
		}
		else {
			$download_code = foncia_get_download_code($pdf_downloader_id, $pdf_post_id);
			if (empty($download_code)) {
				$result_message = __("An error occurred sending your message.", LEYSIN_TEXT_DOMAIN);
			}
		}
	}

	if (!$result_message) {
		$success = foncia_send_download_link_to_user($email, $download_code, $pdf_post_id, $apartment_code);

		if ($success) {
			$result_message = __("Your message has been sent", LEYSIN_TEXT_DOMAIN);
		}
		else {
			$result_message = __("An error occurred sending your message.", LEYSIN_TEXT_DOMAIN);
		}
	}
}

if ($_POST['ajax']) {
	echo json_encode( array(
		'error' => $success ? 0 : 1,
		'message' => $result_message
	) );
	exit;
}


get_header(); ?>

<div class="content-wrapper">
	<div class="<?php echo $success ? 'success' : 'error'; ?>"><?php echo $result_message; ?></div>
</div>

<?php get_footer(); ?>
