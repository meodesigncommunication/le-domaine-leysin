<?php
/**
 * Template Name: PDF download handler
 */

function foncia_show_error($message) {
	get_header(); ?>

	<div class="content-wrapper">
		<div class="error"><?php echo $message; ?></div>
	</div>

	<?php get_footer();
	exit;
}


global $wpdb;

$email = urldecode($_GET['email']);
$download_code = urldecode($_GET['download_code']);

$query = $wpdb->prepare('select pdf.id, pdf.pdf_post_id, person.language
						  from pdf_download pdf
						       join pdf_downloader person on pdf.pdf_downloader_id = person.id
						where person.email = %s
						and pdf.download_code = %s', array($email, $download_code));
$pdf_details = $wpdb->get_row($query);

if (empty($pdf_details)) {
	foncia_show_error(__('Invalid email address or download code', LEYSIN_TEXT_DOMAIN));
}

$post_details = get_post( $pdf_details->pdf_post_id );
if (empty($post_details) or $post_details->post_mime_type != "application/pdf") {
	foncia_show_error(__('Missing or invalid plan details for lot', LEYSIN_TEXT_DOMAIN));
}

// Update the counter
$wpdb->query( $wpdb->prepare("update pdf_download set download_count = download_count + 1 where id = %d", (int) $pdf_details->id ) );

$file = get_attached_file($pdf_details->pdf_post_id);
$lang_file = preg_replace('/pdf$/', $pdf_details->language . '.pdf', $file);
if (file_exists($lang_file)) {
	$file = $lang_file;
}

$filename = basename($file);

header('Content-type: ' . $post_details->post_mime_type);
header('Content-Disposition: attachment; filename="' . $filename . '"');
header('Content-Transfer-Encoding: binary');
header('Content-Length: ' . filesize($file));
header('Accept-Ranges: bytes');

@readfile($file);
