<?php

/* BASE CONTEXT ALL PAGE INCLUDE */

$menus = array();
$wp_navigations = wp_get_nav_menu_items('main');

foreach($wp_navigations as $nav)
{
    if($nav->menu_item_parent != 0)
    {
        $menus[$nav->menu_item_parent]['childs'][$nav->ID]['title'] = $nav->title;
        $menus[$nav->menu_item_parent]['childs'][$nav->ID]['url'] = $nav->url;
    }else {
        $menus[$nav->ID]['title'] = $nav->title;
        $menus[$nav->ID]['url'] = $nav->url;
    }
}

$post = new TimberPost();
$context = Timber::get_context();
$detect = new Mobile_Detect();
$context['post'] = $post;
$context['charset'] = 'UTF-8';
$context['title'] = $post->post_title;
$context['content'] = $post->post_content;
$context['options'] = wp_load_alloptions();
$context['template_path'] = get_template_directory();
$context['template_path_uri'] = get_template_directory_uri();
$context['mobile_device'] = ($detect->isMobile() && !$detect->isTablet()) ? true : false;
$context['menus'] = wp_get_nav_menu_items('main');
$context['current_lang'] = 'fr';
/* / BASE CONTEXT ALL PAGE INCLUDE */

echo '<pre>';
print_r($menus);
echo '</pre>';

//Timber::render( 'templates/page.html.twig' , $context );