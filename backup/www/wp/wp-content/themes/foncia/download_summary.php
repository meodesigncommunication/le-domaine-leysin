<?php
/*
Template Name: download summary
*/

function foncia_summarise_downloads($raw_downloads) {
	$result = array();

	foreach ($raw_downloads as $raw_download) {
		$result[$raw_download->id]['surname']        = $raw_download->surname;
		$result[$raw_download->id]['first_name']     = $raw_download->first_name;
		$result[$raw_download->id]['email']          = $raw_download->email;
		$result[$raw_download->id]['phone']          = $raw_download->phone;
		$result[$raw_download->id]['address']        = $raw_download->address;
		$result[$raw_download->id]['postcode']       = $raw_download->postcode;
		$result[$raw_download->id]['city']           = $raw_download->city;
		$result[$raw_download->id]['country_name']   = $raw_download->country_name;
		$result[$raw_download->id]['language']       = $raw_download->language;
		$result[$raw_download->id]['creation_date']  = $raw_download->creation_date;
		if (!isset($result[$raw_download->id]['download_count'])) {
			$result[$raw_download->id]['download_count'] = array();
			$result[$raw_download->id]['download_url'] = array();
		}
		$result[$raw_download->id]['download_count'][$raw_download->pdf_name] += $raw_download->download_count;
		$result[$raw_download->id]['download_url'][$raw_download->pdf_name] = wp_get_attachment_url($raw_download->pdf_post_id);
	}

	return $result;
}

/* -------------------------------------------------------------------------------------------------------- */

global $wpdb;

$sql = "SELECT a.*
             , pm.meta_value AS pdf_name
             , b.pdf_post_id
             , b.download_count
             , cl.name as country_name
          FROM pdf_downloader a
               JOIN pdf_download b on a.id = b.pdf_downloader_id
               JOIN wp_postmeta pm on b.pdf_post_id = pm.post_id
               LEFT OUTER JOIN country_languages cl on a.country_id = cl.country_id and cl.language_code = 'fr'
         WHERE pm.meta_key = '_wp_attached_file'
           AND a.email not like '%allmeo.com%'
           AND a.surname not in ( 'Holberton', 'test', 'j' )
      ORDER BY a.id desc
             , pm.meta_value
";

$raw_downloads = $wpdb->get_results($sql);

$downloads = foncia_summarise_downloads($raw_downloads);

get_header(); ?>


<article class="content-wrapper" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<section class="one-column">
		<div class="page-title-container"><h2 class="page-title"><?php _e('Downloads', LEYSIN_TEXT_DOMAIN); ?></h2></div>
		<?php if ( post_password_required() ) { ?>
				<?php the_content(); ?>
		<?php }
		else { ?>
			<div class="user-list">
				<table>
					<thead>
						<tr>
							<th class="name"><?php _e('Name', LEYSIN_TEXT_DOMAIN); ?></th>
							<th class="phone"><?php _e('Tel', LEYSIN_TEXT_DOMAIN); ?></th>
							<th class="address"><?php _e('Address', LEYSIN_TEXT_DOMAIN); ?></th>
							<th class="language"><?php _e('Language', LEYSIN_TEXT_DOMAIN); ?></th>
							<th class="files"><?php _e('Files', LEYSIN_TEXT_DOMAIN); ?></th>
							<th class="creation_date"><?php _e('First download', LEYSIN_TEXT_DOMAIN); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php
						$row = 1;
						foreach($downloads as $id => $download) { ?>
							<tr id="user-<?php echo $id; ?>"<?php echo $row++ % 2 == 0 ? ' class="alt"' : ''; ?>>
								<td class="name"><?php echo $download['surname']; ?>, <?php echo $download['first_name']; ?></td>
								<td class="phone"><a href="tel:<?php echo preg_replace('/[^+0-9]/', '', $download['phone']); ?>"><?php echo $download['phone']; ?></a></td>
								<td class="address"><?php echo $download['address']; ?><br>
									<?php echo $download['postcode']; ?><br>
									<?php echo $download['city']; ?><br>
									<?php echo ( $download['country_name'] ? $download['country_name'] . '<br>' : '' ); ?>
									<a href="mailto:<?php echo $download['email']; ?>"><?php echo $download['email']; ?></a>
								</td>
								<td class="language"><?php echo $download['language']; ?></td>
								<td class="files">
									<?php foreach (	$download['download_url'] as $pdf_name => $download_url) {
										echo '<a href="' . $download_url . '" target="_blank">' . $pdf_name . '</a><br>';
									} ?></td>
								<td class="creation_date"><?php echo $download['creation_date']; ?></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		<?php } ?>
	</section>
	<br clear="both" />	
</article><!-- end content-wrapper--><!-- #post-<?php the_ID(); ?> -->


<?php get_footer(); ?>