<?php

define('LEYSIN_TEXT_DOMAIN', 'foncia');

define('HOME_PAGE', 2);
define('CONTACT_PAGE', 16);
define('PLANS_PAGE', 12);

define('GOOGLE_MAPS_API_KEY', 'AIzaSyBT2zVaY9YCAuvfGtP_E_FgOQoyIyc9V9U');

/*** --------------------------------------------------------------------- ***
     Internationalisation
 *** --------------------------------------------------------------------- ***/

load_theme_textdomain( LEYSIN_TEXT_DOMAIN, STYLESHEETPATH . '/languages' );

register_nav_menu( 'secondary', __( 'Secondary Menu', LEYSIN_TEXT_DOMAIN ) );



function foncia_scripts_method() {
    global $post;

	if(!is_admin()) {
        wp_deregister_script( 'jquery' );
        wp_register_script( 'jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js');
        wp_deregister_script('jquery-ui');
        wp_register_script('jquery-ui',("http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"), false, '1.8.23');

        wp_register_script('cycle', get_stylesheet_directory_uri() .'/js/jquery.cycle.all.js', array('jquery'), '1', false);
        wp_register_script('placeholder', get_stylesheet_directory_uri() .'/js/jquery.placeholder.js', array('jquery'), '2.0.7', false);
        wp_register_script('validate', get_stylesheet_directory_uri() .'/js/jquery.validate.js', array('jquery'), '1.11.0', false);
        wp_register_script('tablesorter', get_stylesheet_directory_uri() .'/js/jquery.tablesorter.js', array('jquery'), '2.0.5b', false);
        wp_register_script('cookie', get_stylesheet_directory_uri() .'/js/jquery.cookie.js', array('jquery'), '1.3.1', false);
        wp_register_script('mjscripts', get_stylesheet_directory_uri() . '/js/mjscripts.js', array('jquery', 'validate', 'placeholder', 'cookie', 'tablesorter'), '1.2.2', true);
	
        wp_enqueue_script( 'jquery' );
        wp_enqueue_script( 'cycle' );
        wp_enqueue_script( 'validate' );
        wp_enqueue_script( 'placeholder' );
        wp_enqueue_script( 'cookie' );
        wp_enqueue_script( 'tablesorter' );
        wp_enqueue_script( 'mjscripts' );
        
        wp_enqueue_style('style', get_stylesheet_directory_uri() . '/style.css', array(), filemtime( get_stylesheet_directory() . '/style.css'), 'screen');
    }


    $latitude = get_post_meta($post->ID, "latitude", true);
    $longitude = get_post_meta($post->ID, "longitude", true);
    if (!empty($latitude) and !empty($longitude)) {
        wp_register_script('google.maps', 'https://maps.googleapis.com/maps/api/js?sensor=true&language=' . qtrans_getLanguage() . '&key=' . GOOGLE_MAPS_API_KEY, array(), '3.13.4', false);
        wp_enqueue_script( 'google.maps' );
    }

}

add_action('wp_enqueue_scripts', 'foncia_scripts_method');

add_filter('clean_url', 'foncia_handle_038', 99, 3);
function foncia_handle_038($url, $original_url, $_context) {
    if (strstr($url, GOOGLE_MAPS_API_KEY) !== false) {
        $url = str_replace("&#038;", "&", $url); // or $url = $original_url
    }

    return $url;
}


function get_cft_fieldset_data($like_item, $post_id = null) {
    global $post, $wpdb;
    if ($post_id == null) {
        $post_id = $post->ID;
    }

    $sql = "SELECT * FROM $wpdb->postmeta WHERE post_id = $post_id AND meta_key LIKE '%" . $like_item . "%' ORDER BY meta_id ASC";

    $data_objects = $wpdb->get_results($sql);
    $res = array();
    $ar = array();
    $fieldset = $like_item . 'fieldset';
    if($data_objects) {
        foreach ($data_objects as $data) {
            if ($data->meta_key == $fieldset) continue;
            $ar["$data->meta_key"][]=$data->meta_value;
        }
        if(count($ar)>0) foreach ($ar as $key => $ar2) {
            $i = 0;
            foreach ($ar2 as $value) {
                $res[$i][$key] = $value;
                $i++;
            }
        }
    }

    return $res;
}

// Sort multidimensional array
if( !function_exists('order_array_num') ){
	function order_array_num ($array, $key, $order = "ASC"){
		$tmp = array();
		foreach($array as $akey => $array2){
			$tmp[$akey] = $array2[$key];
		}
		if($order == "DESC"){
			arsort($tmp , SORT_NUMERIC );
		} else {
			asort($tmp , SORT_NUMERIC );
		}
		$tmp2 = array();
		foreach($tmp as $key => $value){
			$tmp2[$key] = $array[$key];
		}
		return $tmp2;
	}
}

// Remove links for non-existent RSS feeds from page headers
add_action('init', 'foncia_remove_feed_hooks');
function foncia_remove_feed_hooks() {
	remove_action( 'wp_head', 'feed_links', 2 );
	remove_action( 'wp_head', 'feed_links_extra', 3 );
	remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
}

function foncia_get_sold_label() {
    return __( 'Sold', LEYSIN_TEXT_DOMAIN );
}

function foncia_get_available_label() {
    return __( 'Available', LEYSIN_TEXT_DOMAIN );
}

add_filter( 'wp_get_attachment_image_attributes', 'foncia_wp_get_attachment_image_attributes' );
function foncia_wp_get_attachment_image_attributes( $attr ) {
	unset($attr['title']);
	return $attr;
}

function foncia_escape_html($in) {
    $escapes = array(
        'Œ' => '&OElig;',  chr(140) => '&OElig;',
        'œ' => '&oelig;',  chr(156) => '&oelig;',
        'À' => '&Agrave;', chr(192) => '&Agrave;',
        'Â' => '&Acirc;',  chr(194) => '&Acirc;',
        'Ä' => '&Auml;',   chr(196) => '&Auml;',
        'Æ' => '&AElig;',  chr(198) => '&AElig;',
        'Ç' => '&Ccedil;', chr(199) => '&Ccedil;',
        'È' => '&Egrave;', chr(200) => '&Egrave;',
        'É' => '&Eacute;', chr(201) => '&Eacute;',
        'Ê' => '&Ecirc;',  chr(202) => '&Ecirc;',
        'Ë' => '&Euml;',   chr(203) => '&Euml;',
        'Î' => '&Icirc;',  chr(206) => '&Icirc;',
        'Ï' => '&Iuml;',   chr(207) => '&Iuml;',
        'Ô' => '&Ocirc;',  chr(212) => '&Ocirc;',
        'Ö' => '&Ouml;',   chr(214) => '&Ouml;',
        'Ù' => '&Ugrave;', chr(217) => '&Ugrave;',
        'Û' => '&Ucirc;',  chr(219) => '&Ucirc;',
        'Ü' => '&Uuml;',   chr(220) => '&Uuml;',
        'ß' => '&szlig;',  chr(223) => '&szlig;',
        'à' => '&agrave;', chr(224) => '&agrave;',
        'â' => '&acirc;',  chr(226) => '&acirc;',
        'ä' => '&auml;',   chr(228) => '&auml;',
        'æ' => '&aelig;',  chr(230) => '&aelig;',
        'ç' => '&ccedil;', chr(231) => '&ccedil;',
        'è' => '&egrave;', chr(232) => '&egrave;',
        'é' => '&eacute;', chr(233) => '&eacute;',
        'ê' => '&ecirc;',  chr(234) => '&ecirc;',
        'ë' => '&euml;',   chr(235) => '&euml;',
        'î' => '&icirc;',  chr(238) => '&icirc;',
        'ï' => '&iuml;',   chr(239) => '&iuml;',
        'ô' => '&ocirc;',  chr(244) => '&ocirc;',
        'ö' => '&ouml;',   chr(246) => '&ouml;',
        'ù' => '&ugrave;', chr(249) => '&ugrave;',
        'û' => '&ucirc;',  chr(251) => '&ucirc;',
        'ü' => '&uuml;',   chr(252) => '&uuml;',
        '£' => '&pound;',  chr(163) => '&pound;',
        '«' => '&laquo;',  chr(171) => '&laquo;',
        '»' => '&raquo;',  chr(187) => '&raquo;'
    );

    return str_replace(array_keys($escapes), array_values($escapes), $in);
}

function foncia_get_building_meta() {
    global $post;

    $result = array();

    $loop = new WP_Query( array(
        'post_type' => 'plans',
        'posts_per_page' => -1
    ) );

    while ($loop->have_posts() ) {
        $loop->the_post();

        $building = array();

        $fieldset_data = get_cft_fieldset_data('floor-');
        $building['floor_data'] =  order_array_num($fieldset_data, 'floor-order', 'ASC');

        $building['chalet_type'] = get_post_meta($post->ID, "chalet_type", true);
        $building['chalet_description'] = get_post_meta($post->ID, "chalet_description_" . qtrans_getLanguage(), true);

        $result[$post->ID] = $building;
    }

    wp_reset_postdata();

    return $result;
}

function foncia_get_availability_description($status) {
    $statuses = array (
        'available' => __('Available', LEYSIN_TEXT_DOMAIN),
        'reserved' => __('Reserved', LEYSIN_TEXT_DOMAIN),
        'sold' => __('Sold', LEYSIN_TEXT_DOMAIN)
    );

    if (!array_key_exists($status, $statuses)) {
        $status = 'sold'; // err on the side of caution
    }

    return $statuses[$status];
}

function experiments_code_add() {
    wp_register_script('google_experiments', 'http://www.google-analytics.com/cx/api.js?experiment=-KI8vB71RviFFNLIi1h8Xw', array(), null, false);
    wp_register_script('experiments', get_stylesheet_directory_uri() . '/js/experiments.js', array('google_experiments'), null, false);

    wp_enqueue_script( 'google_experiments' );
    wp_enqueue_script( 'experiments' );
}

add_action('wp_enqueue_scripts', 'experiments_code_add');

/*** --------------------------------------------------------------------- ***
     Country functions
 *** --------------------------------------------------------------------- ***/

function foncia_get_countries() {
	global $wpdb;

	$language_code = 'fr';
	if (function_exists('qtrans_getLanguage')) {
		$language_code = qtrans_getLanguage();
	}

	$sql = "select c.id as country_id
	             , c.code
	             , cl.name
	             , c.position
			  from countries c
			       left outer join country_languages cl on c.id = cl.country_id
			 where cl.language_code = %s
			   and c.is_active = 'yes'
			 order by ifnull(c.position, 999999)
			        , cl.name";

	$args = array($language_code);

	return $wpdb->get_results($wpdb->prepare( $sql, $args ));
}

function foncia_get_country_by_code($country_code) {
	global $wpdb;

	$language_code = 'fr';
	if (function_exists('qtrans_getLanguage')) {
		$language_code = qtrans_getLanguage();
	}

	$sql = "select c.id as country_id
	             , c.code
	             , cl.name
			  from countries c
			       left outer join country_languages cl on c.id = cl.country_id
			 where c.code = %s
			   and cl.language_code = %s
			   and c.is_active = 'yes' ";

	$args = array(
		$country_code,
		$language_code
	);

	return $wpdb->get_row($wpdb->prepare( $sql, $args ));
}

function foncia_get_country_select() {
	$result = '';

	$countries = foncia_get_countries();
	$separator = "------------------------------------------------------------";
	$first = true;

	$result .= '<select name="country">';
		foreach ($countries as $country) {
			if ($first and empty($country->position)) {
				$separator = ''; // No need to add separator
			}
			elseif (empty($country->position) and !empty($separator)) {
				$result .= '<option value="-" disabled="disabled">' . $separator . '</option>';
				$separator = '';
			}
			$result .= '<option value="' . $country->code . '">' . $country->name . '</option>';
			$first = false;
		}
	$result .= '</select>';

	return $result;
}
