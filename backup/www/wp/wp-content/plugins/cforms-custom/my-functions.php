<?php

// Hooking in to the ugly cforms API

function my_cforms_action($cformsdata) {
	if (!function_exists('cs_save_data')) {
		// 2cpm contact storage not turned on
		return;
	}

	// I can't see an elegant way of this working should the form change :(.  Maybe looking
	// things up in the global $cformsSettings, but seems like more hassle than it's worth

	$data = $cformsdata['data'];

	$language_indices = array(
		'2' => 'en',
		'3' => 'de'
	);

	$field_indices = array(
		'surname'     => '$$$1',
		'first_name'  => '$$$2',
		'email'       => '$$$3',
		'phone'       => '$$$4',
		'address'     => '$$$5',
		'postal_code' => '$$$6',
		'city'        => '$$$7',
		'country'     => '$$$8',
		'message'     => '$$$9'
	);

	$field_values = array();
	foreach ($field_indices as $field_name => $field_index) {
		$field_values[$field_name] = $data[$data[$field_index]];
	}

	$field_values['language'] = 'fr';
	if (isset($language_indices[$cformsdata['id']])) {
		$field_values['language'] = $language_indices[$cformsdata['id']];
	}

    if( ! cs_save_data($field_values) ) {
        $list = '';
        foreach( $field_values as $name => $value ) {
            $list .= sprintf("%s: %s\n", ucfirst(str_replace(array('-', '_'), ' ', $name)), cs_toString($value));
        }

        wp_mail(get_option('admin_email'), 'Data is not synced', "Contact form data was unsuccessfully imported to 2CPM system. Here is it:
$list
Pls contact 2CPM admin to notify him to add the client to the 2CPM system manually.");
    }
}
