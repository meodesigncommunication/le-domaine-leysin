<?php
/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@allmeo.com
*/


class CassiopeeHeader {

	public function __construct() {
		add_action( 'wp_enqueue_scripts', array($this, 'enqueueScriptsAndStyles' ) );
		add_filter( 'body_class', array($this, 'addBodyClass' ) );
	}

	public function enqueueScriptsAndStyles() {
		wp_enqueue_style('cassiopee-header', plugins_url('cassiopee-header/css/cassiopee-header.css'), array(), filemtime(dirname(__FILE__) . '/../css/cassiopee-header.css'));

		wp_enqueue_script('waypoints',        plugins_url('cassiopee-header/js/waypoints.min.js'),        array('jquery'),           '2.0.5');
		wp_enqueue_script('waypoints-sticky', plugins_url('cassiopee-header/js/waypoints-sticky.min.js'), array('waypoints'),        '2.0.5');
		wp_enqueue_script('cassiopee-header', plugins_url('cassiopee-header/js/cassiopee-header.js'),     array('waypoints-sticky'), filemtime(dirname(__FILE__) . '/../js/cassiopee-header.js'));
	}

	public function addBodyClass($classes) {
		$classes[] = 'cassiopee-header-active';

        return $classes;
	}

	public function showHeader() {
			$link = 'http://ledomaineleysin.ch/contact/'; ?>
			<div class="cassiopee-header large">
				<div class="cassiopee-header-large-content">
					<div class="txt-container">
						<h2>Dernières opportunités !</h2>
						<p>
                            Vente aux étrangers et en résidence<br/>secondaire autorisée.
						</p>
						<a style="margin-top: 15px;" class="fancybox-iframe pdf-download-link" href="<?php echo $link ?>" id="btn-form">formulaire de contact</a>
						<p class="contact-info">Laurent Manzoni | t. +41 (0) 79 108 89 58</p>
					</div>
				</div>
			</div><?php
	}
}
