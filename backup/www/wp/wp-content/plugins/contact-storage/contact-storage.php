<?php
/*
Plugin Name: Contacts Storage
Description: Adds admin page to view saved contacts
Author: MasterOfCode LTD
Version: 1.0
Author URI: http://masterofcode.com/
*/

define('CS_API_OPTION_NAME', 'cs_api_url');
define('CS_PAGES_OPTION_NAME', 'cs_form_pages');
define('CS_API_TOKEN', '5y2XkhoVEbauCzfrR1hS');

include 'table.class.php';

register_activation_hook( __FILE__, 'cs_table_setup' );
function cs_table_setup()
{
    CS_Table::setup();
}

add_action('admin_menu','cs_admin_menu_link');
function cs_admin_menu_link()
{
    add_menu_page('Contacts', 'Contacts', 'manage_options', 'cs_admin_page', 'cs_admin_page');
    add_submenu_page('cs_admin_page', 'Settings', 'Settings', 'manage_options', 'cs_settings_page', 'cs_settings_page');
}

function cs_admin_page()
{
    $contacts = new CS_Table();
    $contacts->prepare_items();

    include 'admin_page.php';
}

function cs_settings_page()
{
    if( isset($_POST[CS_API_OPTION_NAME]) ) {
        update_option(CS_API_OPTION_NAME, $_POST[CS_API_OPTION_NAME]);
        update_option(CS_PAGES_OPTION_NAME, $_POST[CS_PAGES_OPTION_NAME]);
    }

    $pages = get_posts(array(
        'post_type'      => 'page',
        'posts_per_page' => -1,
    ));

    $selectedPages = get_option(CS_PAGES_OPTION_NAME);
    if( ! $selectedPages ) $selectedPages = array();

    include 'settings_page.php';
}

add_action('template_redirect', 'cs_save_post_data');
function cs_save_post_data() {
    $post = get_queried_object();
    $page = $post ? $post->ID : '';

    $pages = get_option(CS_PAGES_OPTION_NAME);
    if( ! $pages ) $pages = array();

    if( ! in_array($page, $pages) || ! isset(
        $_POST['first_name'],
        $_POST['surname'],
        $_POST['email']
    ) ) {
        return;
    }

    if( ! cs_save_data($_POST) ) {
        $list = '';
        foreach( $_POST as $name => $value ) {
            $list .= sprintf("%s: %s\n", ucfirst(str_replace(array('-', '_'), ' ', $name)), cs_toString($value));
        }

        wp_mail(get_option('admin_email'), 'Data is not synced', "Contact form data was unsuccessfully imported to 2CPM system. Here is it:
$list
Pls contact 2CPM admin to notify him to add the client to the 2CPM system manually.");
    }
}

/**
 * @param array $formData
 * @return bool
 */
function cs_save_data($formDataReceived)
{
	
	$formData = array();
	
	foreach($formDataReceived as $keyData => $valueData){
		$formData[$keyData] = $valueData;
	}
	
	
    if( ! is_array($formData) || ! isset(
        $formData['first_name'],
        $formData['surname'],
        $formData['email']
    ) ) {
        return false;
    }
	
    /* During Patching : Replaced isset by array_key_exists */
    
    if( array_key_exists('address', $formData) && !array_key_exists('address1', $formData) ) {
        $formData['address1'] = $formData['address'];
        unset($formData['address']);
    }

    if( array_key_exists('phone', $formData) && !array_key_exists('phone1', $formData) ) {
        $formData['phone1'] = $formData['phone'];
        unset($formData['phone']);
    }

    if( array_key_exists('postcode', $formData) && !array_key_exists('postal_code', $formData) ) {
        $formData['postal_code'] = $formData['postcode'];
        unset($formData['postcode']);
    }

    CS_Table::insert(array(
        'first_name' => $formData['first_name'],
        'last_name'  => $formData['surname'],
        'email'      => $formData['email'],
        'address'    => array_key_exists('address1', $formData) ? $formData['address1'] : '',
        'phone'      => array_key_exists('phone1', $formData) ? $formData['phone1'] : '',
        'message'    => array_key_exists('message', $formData) ? $formData['message'] : '',
        'language'   => array_key_exists('language', $formData) ? $formData['language'] : '',
    	/* Patch */
    	'created_at' => date('Y-m-d H:i:s'),
    	'postal_code'=> array_key_exists('postal_code', $formData) ? $formData['postal_code'] : '',
    	'city'		 => array_key_exists('city', $formData) ? $formData['city'] : '',
    	'country'	 => array_key_exists('country', $formData) ? $formData['country'] : '',
    	/*  */
    ));

    $data = array(
        'name'               => $formData['first_name'],
        'surname'            => $formData['surname'],
        'contact_source_url' => defined('SONNIMMO_SOURCE_URL') ? SONNIMMO_SOURCE_URL : site_url(),
        'optional_fields'    => array(),
        'comment'            => null,
        'language'           => null,
        'contact_attributes' => array(
            'email'       => $formData['email'],
            'phone1'      => null,
            'phone2'      => null,
            'address1'    => null,
            'address2'    => null,
            'postal_code' => null,
            'city'        => null,
            'location'    => null,
            'country'     => null,
        ),
    );

    unset($formData['first_name'], $formData['surname'], $formData['email']);

    foreach( $formData as $key => $value ) {
        $value = cs_toString($value);

        if( array_key_exists($key, $data) ) {
            $data[$key] = $value;
        }
        elseif( array_key_exists($key, $data['contact_attributes']) ) {
            $data['contact_attributes'][$key] = $value;
        }
        else {
            $data['optional_fields'][$key] = $value;
        }
    }
	
    foreach( $data as $key => $value ) {
        if( is_null($data[$key]) ) unset($data[$key]);
    }

    foreach( $data['contact_attributes'] as $key => $value ) {
        if( is_null($data['contact_attributes'][$key]) ) unset($data['contact_attributes'][$key]);
    }

    if( count($data['optional_fields']) == 0 ) unset($data['optional_fields']);
    if( count($data['contact_attributes']) == 0 ) unset($data['contact_attributes']);
    
    // wp_mail('your-email@your-domain.com', 'Debug', serialize($data));

    $curl = new WP_Http_Curl();
    $res = $curl->request(get_option(CS_API_OPTION_NAME), array(
        'headers' => array(
            'Accept'       => 'application/json',
            'Content-type' => 'application/json',
        ),
        'method' => 'POST',
        '_redirection' => 0,
        'body' => json_encode(array(
            'api_token' => CS_API_TOKEN,
            'client'    => $data,
        )),
    ));

	if ( is_wp_error($res) ) {
		return false;
	}

    return $res['response']['code'] == 201;
}

function cs_toString($value)
{
    if( is_array($value) ) {
        return implode(', ', $value);
    }
    elseif( is_object($value) ) {
        return implode(', ', (array)$value);
    }
    else return $value;
}
