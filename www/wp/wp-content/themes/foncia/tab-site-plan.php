<?php
	$sold_label = foncia_get_sold_label();
	$available_label = foncia_get_available_label();
	$appartment_classes = array(
		1 => 'appartment-left',
		2 => 'appartment-center',
		3 => 'appartment-right',
		4 => 'appartment-behind'
	);


	$area_labels = array(
		'apartment' => __('Apartment area', LEYSIN_TEXT_DOMAIN),
		'terrace'   => __('Terrace area', LEYSIN_TEXT_DOMAIN),
		'balcony'   => __('Balcony area', LEYSIN_TEXT_DOMAIN),
		'garden'   => __('Garden area', LEYSIN_TEXT_DOMAIN),
		'ponderee'  => __('Surface area', LEYSIN_TEXT_DOMAIN)
	);

	$buildings = foncia_get_building_meta();

?>
<div class="site-plan tab">
	<div id="site-plan">
		<section class="one-column">
			<div class="site-overview">
				<img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/plan_situation.jpg" width="775" height="426" alt="Site" usemap="#site_map">
				<map id="site_map" name="site_map">
					<area class="building_link" shape="rect" coords="326,57,426,210" href="#building_1" >
					<area class="building_link" shape="rect" coords="472,22,566,174" href="#building_2" >
				</map>
			</div>

			<div class="chalet-container">
				<div class="availablility_labels">
					<div class="disponible"><?php echo $available_label; ?></div>
					<div class="vendu"><?php echo $sold_label; ?></div>
				</div>
				<ul>
					<?php
					$building_number = 1;
					foreach ($buildings as $building_details) {
						$fieldset_data      = $building_details['floor_data'];
						$chalet_type        = $building_details['chalet_type'];
						$chalet_description = $building_details['chalet_description'];
						?>
						<li class="chalet" id="building_<?php echo $building_number; ?>">
							<div class="specification">
								<h3>Chalet <?php echo $chalet_type; ?></h3>
								<strong><?php echo $chalet_description; ?></strong>

								<?php
								$floor_number = 1;
								if ($fieldset_data) :
									foreach($fieldset_data as $item) :
										if(!$item['floor-floor_name']) {
											continue;
										} ?>
										<div class="apartment-references">
											<?php foreach (array_keys($appartment_classes) as $apartment_number) {
												$floor_details = '<ul>';
												$floor_details .= '<li><b>' . __('Apartment', LEYSIN_TEXT_DOMAIN) . ' ' . $item['floor-code' . $apartment_number] . '</b></li>';
												foreach ($area_labels as $field => $label) {
													if ($item['floor-area-' . $field . $apartment_number ]) {
														$floor_details .= '<li>' . $label . ' : ' . $item['floor-area-' . $field . $apartment_number ] . 'm<sup>2</sup></li>';
													}
												}
												$floor_details .= '</ul>'; ?>
												<a class="building-number-<?php echo $building_number; ?> floor-number-<?php echo $floor_number; ?> reference-appartment-<?php echo $apartment_number; ?>" data-avail="<?php echo $item['floor-vendu' . $apartment_number ]; ?>"><?php echo $floor_details; ?></a>
											<?php } ?>

										</div>
										<?php $floor_number++;
									endforeach;
								endif; ?>

							</div><!-- end specification-->

							<div class="specification-img-container chalet_type<?php echo $chalet_type; ?>">

								<div class="appartments-floors">

									<?php if ($building_number == 1) {?>
										<a class="lot_link appartment2 fifth-floor" href="#lot_<?php echo $building_number; ?>_5_2">
											<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/appartment2.gif" alt="5" data-appartment="2" data-building="<?php echo $building_number; ?>" />
										</a>
									<?php } ?>

									<a class="lot_link appartment1 fourth-floor" href="#lot_<?php echo $building_number; ?>_4_1">
										<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/appartment1.gif" alt="4" data-appartment="1" data-building="<?php echo $building_number; ?>" />
									</a>
									<a class="lot_link appartment2 fourth-floor" href="#lot_<?php echo $building_number; ?>_4_2">
										<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/appartment2.gif" alt="4" data-appartment="2" data-building="<?php echo $building_number; ?>" />
									</a>
									<?php if ($building_number == 1) {?>
									<a class="lot_link appartment4 fourth-floor" href="#lot_<?php echo $building_number; ?>_4_4">
										<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/appartment1.gif" alt="4" data-appartment="4" data-building="<?php echo $building_number; ?>" />
									</a>
									<?php }?>


									<a class="lot_link appartment1 third-floor" href="#lot_<?php echo $building_number; ?>_3_1">
										<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/appartment1.gif" alt="3" data-appartment="1" data-building="<?php echo $building_number; ?>" />
									</a>
									<a class="lot_link appartment2 third-floor" href="#lot_<?php echo $building_number; ?>_3_2">
										<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/appartment2.gif" alt="3" data-appartment="2" data-building="<?php echo $building_number; ?>" />
									</a>
									<a class="lot_link appartment3 third-floor" href="#lot_<?php echo $building_number; ?>_3_3">
										<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/appartment1.gif" alt="3" data-appartment="3" data-building="<?php echo $building_number; ?>" />
									</a>

									<?php if ($building_number == 1) {?>
									<a class="lot_link appartment4 third-floor" href="#lot_<?php echo $building_number; ?>_3_4">
										<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/appartment1.gif" alt="3" data-appartment="4" data-building="<?php echo $building_number; ?>" />
									</a>
									<?php }?>


									<a class="lot_link appartment1 second-floor" href="#lot_<?php echo $building_number; ?>_2_1">
										<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/appartment1.gif" alt="2" data-appartment="1" data-building="<?php echo $building_number; ?>" />
									</a>
									<a class="lot_link appartment2 second-floor" href="#lot_<?php echo $building_number; ?>_2_2">
										<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/appartment2.gif" alt="2" data-appartment="2" data-building="<?php echo $building_number; ?>" />
									</a>
									<a class="lot_link appartment3 second-floor" href="#lot_<?php echo $building_number; ?>_2_3">
										<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/appartment1.gif" alt="2" data-appartment="3" data-building="<?php echo $building_number; ?>" />
									</a>

									<?php if ($building_number == 1) {?>
									<a class="lot_link appartment4 second-floor" href="#lot_<?php echo $building_number; ?>_2_4">
										<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/appartment1.gif" alt="2" data-appartment="4" data-building="<?php echo $building_number; ?>" />
									</a>
									<?php }?>
									<a class="lot_link appartment1 first-floor" href="#lot_<?php echo $building_number; ?>_1_1">
										<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/appartment1.gif" alt="1" data-appartment="1" data-building="<?php echo $building_number; ?>" />
									</a>
									<a class="lot_link appartment2 first-floor" href="#lot_<?php echo $building_number; ?>_1_2">
										<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/appartment2.gif" alt="1" data-appartment="2" data-building="<?php echo $building_number; ?>" />
									</a>
									<a class="lot_link appartment3 first-floor" href="#lot_<?php echo $building_number; ?>_1_3">
										<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/appartment1.gif" alt="1" data-appartment="3" data-building="<?php echo $building_number; ?>" />
									</a>

									<?php if ($building_number == 2) {?>
									<a class="lot_link appartment4 first-floor" href="#lot_<?php echo $building_number; ?>_1_4">
										<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/appartment1.gif" alt="1" data-appartment="4" data-building="<?php echo $building_number; ?>" />
									</a>
									<?php }?>
								</div><!-- end appartments-floors-->
							</div><!-- end specification-img-container-->
						</li>
					<?php
					$building_number++;
				} ?>
				</ul>
			</div>
		</section>

		<div class="clear"></div>
	</div>
</div>
