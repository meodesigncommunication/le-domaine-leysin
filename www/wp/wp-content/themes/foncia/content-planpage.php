<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>

<script>
  (function($){

		variations = [
			'<?php echo get_permalink(get_page_by_path("form-1")) ?>',
			'<?php echo get_permalink(get_page_by_path("form-2")) ?>'
		];
		
	})(jQuery);
</script>

<?php
$form_labels = array(
	'form_title' => __('To receive your download link,<br>please provide the following information:', LEYSIN_TEXT_DOMAIN),

	'surname'    => __('Surname', LEYSIN_TEXT_DOMAIN),
	'first_name' => __('First name', LEYSIN_TEXT_DOMAIN),
	'email'      => __('email', LEYSIN_TEXT_DOMAIN),
	'phone'      => __('Tel', LEYSIN_TEXT_DOMAIN),
	'address'    => __('Address', LEYSIN_TEXT_DOMAIN),
	'postcode'   => __('Postcode', LEYSIN_TEXT_DOMAIN),
	'city'       => __('City', LEYSIN_TEXT_DOMAIN),

	'required'   => __('required fields', LEYSIN_TEXT_DOMAIN),

	'send'       => __('send', LEYSIN_TEXT_DOMAIN),

	'sent'       => __('The download link for your PDF has been sent to your email address', LEYSIN_TEXT_DOMAIN),
	'ok'         => __('OK', LEYSIN_TEXT_DOMAIN),

	'send_to'    => __('The download link for your PDF will be sent to your email address', LEYSIN_TEXT_DOMAIN),

	'change_address' => __("Change", LEYSIN_TEXT_DOMAIN)
);

?>



<article id="post-<?php the_ID(); ?>" <?php post_class('content-wrapper'); ?>>
	<div class="tab-choosers">
		<a class="tab-chooser" id="tab-chooser-list" href="#apartment-list"><img src="<?php bloginfo('stylesheet_directory');?>/images/listing_icon.jpg"></a>
		<a class="tab-chooser" id="tab-chooser-map" href="#map-area"><?php _e('Area map', LEYSIN_TEXT_DOMAIN); ?></a>
		<a class="tab-chooser active" id="tab-chooser-site" href="#site-plan"><?php _e('Chalets', LEYSIN_TEXT_DOMAIN); ?></a>
		<a class="tab-chooser" id="tab-chooser-building" href="#floor" style="display: none;"><?php _e('Floor', LEYSIN_TEXT_DOMAIN); ?></a>
	</div>
	<div class="clear"></div>

	<?php get_template_part( 'tab-apartment-list' ); ?>
	<?php get_template_part( 'tab-map-area' ); ?>
	<?php get_template_part( 'tab-site-plan' ); ?>
	<?php get_template_part( 'tab-floor' ); ?>


	<a id="form_opener" class="fancybox" href="#pdf_download" style="display: none;">&nbsp;</a>

	<div>
		<?php edit_post_link( __( 'Edit', 'twentyeleven' ), '<span class="edit-link">', '</span>' ); ?>
	</div><!-- .entry-meta -->
</article><!-- end content-wrapper--><!-- #post-<?php the_ID(); ?> -->
