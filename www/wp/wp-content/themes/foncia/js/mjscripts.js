(function($){
	var $tabs = $('.tab'),
		$tab_choosers = $('.tab-chooser'),
		map = null;

	// Tabs
	$tab_choosers.click(function(e) {
		e.preventDefault();
		var $this = $(this);
		$tab_choosers.removeClass('active');
		$this.addClass('active');
		var destination = $this.attr('href');
		var $clicked_tab = $(destination).closest('.tab');
		$tabs.hide();
		$clicked_tab.show();
		if ($clicked_tab.hasClass('map-area') && typeof map !== "undefined") {
			var centre = map.getCenter();
			google.maps.event.trigger(map, 'resize');
			map.setCenter(centre);
		}

		if (typeof _gaq !== "undefined") {
			destination = destination.substring(1);
			_gaq.push(['_trackPageview', window.location.pathname + destination]);
		}
	});

	$(".building_link").click(function(e) {
		e.preventDefault();
		$this = $(this);
		var destination = $this.attr('href');

		$tabs.hide();
		$('.tab.site-plan').show();
		$tab_choosers.removeClass('active');
		$('#tab-chooser-site').addClass('active');

		$('html, body').animate({
			scrollTop: $(destination).offset().top
		}, 'slow');
	});

	$(".lot_link").click(function(e) {
		e.preventDefault();
		$this = $(this);
		var destination = $this.attr('href'),
			apartment_details = destination.split('_');

		if (apartment_details.length < 4) {
			return;
		}

		var building_number = apartment_details[1],
			floor_number = apartment_details[2],
			apartment_number = apartment_details[3],
			$floor = $('#floor_' + building_number + '_' + floor_number);


		$tabs.hide();
		$('.tab.floor-tab').show();
		$tab_choosers.removeClass('active');
		$('#tab-chooser-building').show().addClass('active').html( $floor.data('building-name') + ' / <span class="floor-name">' + $floor.data('floor-name') + '</span>');
		$('.floor').hide();
		$floor.show();
		$floor.find('.green.floorNum' + floor_number + ':not(:has(.red' + apartment_number + '))').children('.layerHover').addClass('active' + apartment_number);
	});



	function validate_element(element, check_autocomplete) {
		var isvalid = $(element).valid(),
			name = $(element).attr("name");

		if (isvalid) {
			$('#' + name + '_state').removeClass('invalid').addClass('valid');
			if (check_autocomplete) {
				validate_autocomplete_fields();
			}
			var invalid_fields = $('#customer_registration_form .input_state').not('.valid');
			if (invalid_fields.length == 0) {
				$form_submit_buttons.removeAttr('disabled');
			}
			else {
				$form_submit_buttons.attr('disabled','disabled');
			}
		}
		else  {
			$('#' + name + '_state').removeClass('valid').addClass('invalid');
			$form_submit_buttons.attr('disabled','disabled');
		}
	}

	function validate_autocomplete_fields() {
		var $unchecked_fields = $('#customer_registration_form .input_state:visible');
		$unchecked_fields.each(function(n) {
			var element = $(this).siblings('[type="text"]');
			if (element.val()) {
				validate_element(element, false);
			}
		});
	}

	var $slideshow_container = null,
		$reg_form_validator = null;

	$(".slideshow").after('<div class="nav-container"><div id="nav">').cycle({fx:'fade', speed: 2000, timeout: 5000, pager:'#nav'});

	$(".specification-img-container a").hover(
		function(){
			var buildingNumber = $(this).find('img').data('building');
			var floorNumber = $(this).find('img').attr('alt');
			var appartmentNumber = $(this).find('img').data('appartment');
			var CurFloor = $(".floor-number-"+floorNumber);
			var selected = $(".building-number-"+buildingNumber+ ".floor-number-"+floorNumber+ ".reference-appartment-"+appartmentNumber);
			var avail = selected.data('avail');
			$(this).addClass(avail);
			selected.stop(true, true).fadeOut(500);
			selected.show();
		},
		function(){
			var buildingNumber = $(this).find('img').data('building');
			var floorNumber = $(this).find('img').attr('alt');
			var appartmentNumber = $(this).find('img').data('appartment');
			var CurFloor = $(".floor-number-"+floorNumber);
			var selected = $(".building-number-"+buildingNumber+ ".floor-number-"+floorNumber+ ".reference-appartment-"+appartmentNumber);
			var avail = selected.data('avail');
			$(this).removeClass(avail);
			selected.stop(true, true).fadeIn(1000);
			selected.hide();
		}
	);

	$('.apartment-list .download a').click(function(e) {
		var apartment_code = $(this).closest('.download-wrapper').data('code');
		$('#pdf_apartment_code').val(apartment_code);
		pdf_apartment_code = apartment_code;
	});

	$('.appartment a').click(function(e) {
		var apartment_code = $(this).closest('.appartment').data('code');
		$('#pdf_apartment_code').val(apartment_code);
		pdf_apartment_code = apartment_code;
	});

	// Add placeholder functionality for browsers that don't support it
	$('input, textarea').placeholder();

	var $form_submit_buttons = $('#customer_registration_form [type="submit"]');

	// pdf download form validation
	$reg_form_validator = $('#customer_registration_form').validate({
		rules :{
			surname: "required",
			first_name: "required",
			email:  {
				required: true,
				email: true
			},
			phone: "required",
			message: "required",
			address: "required",
			postcode: "required",
			city: "required"
		},
		messages: {
			surname: "",
			first_name: "",
			email: "",
			phone: "",
			message: "",
			address: "",
			postcode: "",
			city: ""
		},
		errorClass: "invalid",
		onfocusout: function(element) {
			validate_element(element, true);
		}
	});

	$('#customer_registration_form').submit(function (e) {
		e.preventDefault();
		var $form = $('#customer_registration_form'),
			data = $form.serialize() + "&ajax=1",
			success = false,
			action = $form.attr('action'),
			email = $form.find('input.email').val();

		if (typeof _gaq !== "undefined") {
			_gaq.push(['_trackPageview', 'pdf_form_submitted']);
		}


		$.ajax({
			type: "POST",
			async: false,
			url: action,
			data: data,
			dataType: "json",
			success: function(retval) {
				$form.find('input:text, textarea').val('').blur().removeClass('invalid');
				success = true;
			},
			error: function(jqXHR, textStatus, errorThrown) {
				alert("Apologies, an error occurred sending your data to our server.");
			}
		});

		if (success) {
			$.cookie('pdf_email', email, { expires: 7, path: '/' });
			$('#pdf_download .pdf_email_address').html(email);
			$('#pdf_download_form').hide();
			$('#pdf_download_thanks').show();
		}
	});

	$('#pdf_download_thanks').click(function(e){
		$.fancybox.close();
	});

	$('#customer_registration_form .change').click(function (e) {
		e.preventDefault();
		$.removeCookie('pdf_email');
		$('#pdf_download .initial').show();
		$('#pdf_download .subsequent').hide();
	});

    // add tablesorter parser for the lot number column
    $.tablesorter.addParser({
		id: 'lots',
		is: function(s) {
			return false; // so parser is not auto detected
		},
		format: function(s) {
			var parts = s.split('-');
			if (parts.length != 2) {
				return s;
			}
			return parts[0] + "-" + ("0000" + parts[1]).slice(-4);
		},
		type: 'text'
	});

	// add tablesorter parser for the floor column
	$.tablesorter.addParser({
		id: 'floor',
		is: function(s) {
			return false; // so parser is not auto detected
		},
		format: function(s) {
			return floor_order[s];
			// if ( s.toLowerCase() == "rdc" || s.toLowerCase() == "g" ) {
			// 	return 0;
			// }
			// return parseInt(s, 10);
		},
		type: 'numeric'
	});

    $("#apartment-list table").tablesorter({
		headers: {
			1: { sorter:'floor' },
			2: { sorter:'lots' },
			4: { sorter:'digit' }
		}
	});

    var docHeight = $(document).height();
    $theWindow = $(window),
    $bg = $("#bg");

    // Use javascript-enabled css where appropriate
    $("html.no-js").removeClass('no-js').addClass('js');

    $(window).load(function() {
		if (typeof latitude !== "undefined" && typeof longitude !== "undefined") {
			var centre = new google.maps.LatLng(latitude, longitude);

			var mapOptions = {
				zoom: 15,
				center: centre,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			map = new google.maps.Map(document.getElementById('map'), mapOptions);

			var image = {
				url: '/wp/wp-content/themes/foncia/images/ledomaineleysinlogo.png',
				size: new google.maps.Size(129, 125),
				origin: new google.maps.Point(0,0),
				anchor: new google.maps.Point(64, 122)
			};

			var marker = new google.maps.Marker({
				position: centre,
				map: map,
				icon: image,
				title: "Le Domaine du Parc, Leysin",
				zIndex: 5
			});

		}

		// If URL contains an internal link corresponding to a tab, click it
		if (location.hash) {
			var $target = $('a.tab-chooser[href="' + location.hash + '"]:first');
			if ($target.length > 0) {
				$target.click();
			}
		}

        var aspectRatio = $bg.width() / $bg.height();

        function resizeBg() {
            if ( ($theWindow.width() / $theWindow.height()) < aspectRatio ) {
                $bg.removeClass().addClass('bgheight');
            } else {
                $bg.removeClass().addClass('bgwidth');
            }
        }

        $theWindow.resize(function() {
            resizeBg();
        }).trigger("resize");

        $bg.css('visibility','visible');
    });


	try {
		var popup_url = variations[variation];
		// fancybox
		$(".fancybox-form").fancybox({
			'transitionIn' : 'elastic',
			'transitionOut' : 'elastic',
			'speedIn' : 500,
			'speedOut' : 400,
			'overlayShow' : true,
			'overlayOpacity' : 0.7,
			'type': 'ajax',
			'href': popup_url,
			onComplete: function() {
				if(variation == 0) {
					if (typeof _gaq !== "undefined") {
						_gaq.push(['_trackPageview', 'form-1']);
					}

					$form_submit_buttons = $('#customer_registration_form [type="submit"]');

					// Add placeholder functionality for browsers that don't support it
					$('#customer_registration_form input, #customer_registration_form textarea').placeholder();

					$reg_form_validator = $('#customer_registration_form').validate({
						rules :{
							surname: "required",
							first_name: "required",
							email:  {
								required: true,
								email: true
							},
							phone: "required",
							message: "required",
							address: "required",
							postcode: "required",
							city: "required"
						},
						messages: {
							surname: "",
							first_name: "",
							email: "",
							phone: "",
							message: "",
							address: "",
							postcode: "",
							city: ""
						},
						errorClass: "invalid",
						onfocusout: function(element) {
							validate_element(element, true);
						}
					});

					$('#customer_registration_form').submit(function (e) {
						e.preventDefault();
						$('#pdf_apartment_code').val(pdf_apartment_code);
						var $form = $('#customer_registration_form'),
							data = $form.serialize() + "&ajax=1",
							success = false,
							action = $form.attr('action'),
							email = $form.find('input.email').val();

						if (typeof _gaq !== "undefined") {
							_gaq.push(['_trackPageview', 'pdf_form_submitted']);
						}


						$.ajax({
							type: "POST",
							async: false,
							url: action,
							data: data,
							dataType: "json",
							success: function(retval) {
								$form.find('input:text, textarea').val('').blur().removeClass('invalid');
								success = true;
								if (typeof _gaq !== "undefined") {
									_gaq.push(['_trackPageview', 'form-handler']);
								}
							},
							error: function(jqXHR, textStatus, errorThrown) {
								alert("Apologies, an error occurred sending your data to our server.");
							}
						});

						if (success) {
							$.cookie('pdf_email', email, { expires: 7, path: '/' });
							$('#pdf_download .pdf_email_address').html(email);
							$('#pdf_download_form').hide();
							$('#pdf_download_thanks').show();
						}
					});

					$('#pdf_download_thanks').click(function(e){
						$.fancybox.close();
					});

					$('#customer_registration_form .change').click(function (e) {
						e.preventDefault();
						$.removeCookie('pdf_email');
						$('#pdf_download .initial').show();
						$('#pdf_download .subsequent').hide();
					});
				} else if(variation == 1) {
					if (typeof _gaq !== "undefined") {
						_gaq.push(['_trackPageview', 'form-2']);
					}

					function send_download_link() {
						$('body').addClass('waiting');

						var $form = $('#pdf_download_form'),
							data = $form.serialize() + "&ajax=1",
							success = false,
							action = $form.attr('action');

						$.ajax({
							type: "POST",
							async: false,
							url: action,
							data: data,
							dataType: "json",
							error: function(jqXHR, textStatus, errorThrown) {
								alert("Apologies, an error occurred sending your data to our server.");
							}
						});

						$('.pdf_popup').hide();
						$('#pdf_download_thanks').show();
						$('body').removeClass('waiting');
					}

					$('a.pdf_download').click(function(e) {
						// Set the PDF ID in the form
						var lot_id = $(this).attr('id');
						lot_id = lot_id.replace('download_plan_', '');
						$('#pdf_download_pages input.lot_id').val(lot_id);

						// Check the cookie
						$('.pdf_popup').hide();
						var email = $.cookie('pdf_email');
						if (email) {
							$('#pdf_download_form input.email').val(email);
							$('.pdf_email_address').html(email);
							$('#pdf_confirm_email').show();
						}
						else {
							$('#pdf_download').show();
							if (typeof _gaq !== "undefined") {
								_gaq.push(['_trackPageview', 'pdf_form_displayed']);
							}
						}

						// Register the download attempt with Google Analytics
						if (typeof _gaq !== "undefined") {
							var destination = $(this).attr('data-url');
							_gaq.push(['_trackPageview', destination]);
						}
					});

					$('#pdf_confirm_email button.change').click(function(e) {
						e.preventDefault();
						$.removeCookie('pdf_email');
						$('.pdf_popup').hide();
						$('#pdf_download').show();
					});

					$('#pdf_confirm_email button[type="submit"]').click(function(e) {
						e.preventDefault();
						send_download_link();
					});

					$('#pdf_download_thanks').click( function(e){
						e.preventDefault();
						$.fancybox.close();
					});

					// Add placeholder functionality for browsers that don't support it
					$('#customer_registration_form_2 input, #customer_registration_form_2 textarea').placeholder();

					// pdf download form validation
					$('#customer_registration_form_2').validate({
						rules :{
							surname: "required",
							first_name: "required",
							email:  {
								required: true,
								email: true
							},
							phone: "required",
							message: "required",
							address: "required",
							postcode: "required",
							city: "required"
						},
						messages: {
							surname: "",
							first_name: "",
							email: "",
							phone: "",
							message: "",
							address: "",
							postcode: "",
							city: ""
						},
						errorClass: "invalid"
					});

					$('#customer_registration_form_2').submit(function (e) {
						e.preventDefault();
						$('#pdf_apartment_code').val(pdf_apartment_code);
						$('body').addClass('waiting');

						var $form = $('#customer_registration_form_2'),
							data = $form.serialize() + "&ajax=1",
							success = false,
							action = $form.attr('action'),
							email = $form.find('input.email').val();

						var valid = true;
						var validator = $form.validate();
						$form.find('input').each(function() {
							valid &= validator.element(this);
						});
							
						if (!valid) {
							$('body').removeClass('waiting');
							return;
						}			

						if (typeof _gaq !== "undefined") {
							_gaq.push(['_trackPageview', 'pdf_form_submitted']);
							_gaq.push(['_trackEvent', 'Registration form', 'Submitted', 'Form 2']);
						}

						$('#pdf_download_form input.surname').val(   $form.find('input.surname').val());
						$('#pdf_download_form input.first_name').val($form.find('input.first_name').val());
						$('#pdf_download_form input.phone').val(     $form.find('input.phone').val());
						$('#pdf_download_form input.address').val(   $form.find('input.address').val());
						$('#pdf_download_form input.postcode').val(  $form.find('input.postcode').val());
						$('#pdf_download_form input.city').val(      $form.find('input.city').val());

						$.ajax({
							type: "POST",
							async: false,
							url: action,
							data: data,
							dataType: "json",
							success: function(retval) {
								$('body').removeClass('waiting');
								$form.find('input:text, textarea').val('').blur().removeClass('invalid');

								if (typeof _gaq !== "undefined") {
									_gaq.push(['_trackPageview', 'form-handler']);
								}
							},
							error: function(jqXHR, textStatus, errorThrown) {
								$('body').removeClass('waiting');
								alert("Apologies, an error occurred sending your data to our server.");
							}
						});

						$.cookie('pdf_email', email);
						$('.pdf_email_address').html(email);
						$('#pdf_download_form input.email').val(email);

						send_download_link();
					});
				}
			}
		});
	} catch(e) {}

})(jQuery);
