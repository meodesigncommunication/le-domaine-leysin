<?php
$buildings = foncia_get_building_meta();

$chalet_b_post_id = 47;

if (!empty($buildings[$chalet_b_post_id])) {
	$floors = array();

	$floor_number = 1;
	foreach($buildings[$chalet_b_post_id]['floor_data'] as $item) {
		if(!$item['floor-floor_name']) {
			continue;
		}
		$floors[$item['floor-floor_name']] = $floor_number++;
	}

	?><script type="text/javascript">
		floor_order = <?php echo json_encode($floors); ?>;
	</script><?php
} ?>

<div class="apartment-list tab">
	<div id="apartment-list">
		<table>
			<thead>
				<tr>
					<th class="building"><?php _e('Building', LEYSIN_TEXT_DOMAIN); ?></th>
					<th class="floor"><?php _e('Floor', LEYSIN_TEXT_DOMAIN); ?></th>
					<th class="lot"><?php _e('Lot Number', LEYSIN_TEXT_DOMAIN); ?></th>
					<th class="rooms"><?php _e('Rooms', LEYSIN_TEXT_DOMAIN); ?></th>
					<th class="area"><?php _e('Surface area', LEYSIN_TEXT_DOMAIN); ?>&nbsp;/&nbsp;<span class="lower">m</span><sup>2</sup></th>
					<th class="availability"><?php _e('Availability', LEYSIN_TEXT_DOMAIN); ?></th>
					<th class="download"><div class="calltoaction"><?php _e('Download apartment plan', LEYSIN_TEXT_DOMAIN); ?></div></th>
				</tr>
			</thead>
			<tbody><?php
				$building_number = 1;
				foreach ($buildings as $building) {
					$building_name = $building['chalet_type'];
					$floor_number = 1;
					foreach($building['floor_data'] as $item) {
						if(!$item['floor-floor_name']) {
							continue;
						}
						for ($apartment_number = 1; $apartment_number <= 4; $apartment_number++) {
							if(!$item['floor-code' . $apartment_number]) {
								continue;
							}
							$availability = ( $item['floor-vendu' . $apartment_number] == 'disponible' ? 'available' : 'sold' );
							$apartment_code = $item['floor-code' . $apartment_number];
							$rooms = $item['floor-rooms' . $apartment_number];
							$area = $item['floor-area-ponderee' . $apartment_number];

							$open_link = '<a class="lot_link" href="#lot_' . $building_number . '_' . $floor_number . '_' . $apartment_number . '">';
							$close_link = '</a>';

							?>
							<tr class="<?php echo $availability; ?>">
								<td class="building"><a class="building_link" href="#building_<?php echo $building_number; ?>"><?php echo $building_name; ?></a></td>
								<td class="floor"><?php echo $open_link; echo $item['floor-floor_name']; echo $close_link; ?></td>
								<td class="lot"><?php   echo $open_link; echo $apartment_code; echo $close_link; ?></td>
								<td class="rooms"><?php echo $open_link; echo $rooms; echo ' <span class="lower">p.</span>'; echo $close_link; ?></td>
								<td class="area"><?php  echo $open_link; echo $area; echo ' <span class="lower">m</span><sup>2</sup>'; echo $close_link; ?></td>
								<td class="availability"><?php echo $open_link; echo foncia_get_availability_description($availability); echo $close_link; ?></td>
								<td class="download"><div class="download-wrapper" style="line-height: 1px; height:37px; width:36px; margin: 0 auto; background-image: url(<?php echo get_bloginfo('stylesheet_directory'); ?>/images/listing_download.png);" data-code="<?php echo $item['floor-code' . $apartment_number]; ?>"><a class="fancybox-form" href="#pdf_download" style="height:37px; width:36px;"></a></div></td>
							</tr><?php
						}
						$floor_number++;
					}
				} ?>
			</tbody>
		</table>
		<div class="clear"></div>
	</div>
</div>