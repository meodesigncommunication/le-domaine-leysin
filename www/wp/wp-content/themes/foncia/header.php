<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
global $cassiopee_header;
?><!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?> class="no-js">
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?> class="no-js">
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?> class="no-js">
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?> class="no-js">
<!--<![endif]-->
<head>
<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:600,400,700" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Rokkitt:400,700" rel="stylesheet" type="text/css">
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyeleven' ), max( $paged, $page ) );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); echo '?' . filemtime( get_stylesheet_directory() . '/style.css'); ?>" />
<!--[if lt IE 9]>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
<script type="text/javascript">
jQuery(document).ready(function($){

	$(".specification-img-container a").click(function(e){
		var apartmentActive = $(this).find('img').data('appartment');
		var slideNumber = $(this).find('img').attr('alt');
		var slideId = $(this).find('img').data('building');
		$("#slider-id"+slideId+" .floorNum"+slideNumber).addClass("active"+apartmentActive);
		$(".red"+apartmentActive).parent().removeClass("active"+apartmentActive);
		e.preventDefault();
	});

	$(".appartment-left").mouseout(function(){
		$(this).parent().children('.layerHover').removeClass("active1");
	});
	$(".appartment-center").mouseout(function(){
		$(this).parent().children('.layerHover').removeClass("active2");
	});
	$(".appartment-right").mouseout(function(){
		$(this).parent().children('.layerHover').removeClass("active3");
	});
	
	$(".appartment-behind").mouseout(function(){
		$(this).parent().children('.layerHover').removeClass("active4");
	});

	$(".appartment-left.disponible a").hover(
		function(){ $(this).closest('.layer-over').children('.layerHover').addClass("hov1"); },
		function(){ $(this).closest('.layer-over').children('.layerHover').removeClass("hov1"); }
	);

	$(".appartment-center.disponible a").hover(
		function(){ $(this).closest('.layer-over').children('.layerHover').addClass("hov2"); },
		function(){ $(this).closest('.layer-over').children('.layerHover').removeClass("hov2"); }
	);

	$(".appartment-right.disponible a").hover(
		function(){ $(this).closest('.layer-over').children('.layerHover').addClass("hov3");},
		function(){ $(this).closest('.layer-over').children('.layerHover').removeClass("hov3");}
	);
	
	$(".appartment-behind.disponible a").hover(
		function(){ $(this).closest('.layer-over').children('.layerHover').addClass("hov4");},
		function(){ $(this).closest('.layer-over').children('.layerHover').removeClass("hov4");}
	);

	$(".fancybox").fancybox({
		onStart: function(currentArray, currentIndex, currentOpts) {
			$('#pdf_download_form').show();
			$('#pdf_download_thanks').hide();
			var email = $.cookie('pdf_email');
			$('#pdf_download .pdf_email_address').html(email);
			$('#pdf_download_form input.email').val(email);
			if (email) {
				$('#pdf_download .initial').hide();
				$('#pdf_download .subsequent').show();
				$('#customer_registration_form .subsequent [type="submit"]').removeAttr('disabled');
			}
			else {
				$('#pdf_download .initial').show();
				$('#pdf_download .subsequent').hide();
				if (_gaq) {
					_gaq.push(['_trackPageview', 'pdf_form_displayed']);
				}
			}
		}
	});
});
</script>
<?php
$show_secondary_menu = get_post_meta($post->ID, "show_secondary_menu", true);
if ($show_secondary_menu == "on") {
	global $cta_version;
	$cta_version = 2;
}
if ($_SERVER['SERVER_ADDR'] != "127.0.0.1") { ?>
	<script type="text/javascript">

		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-36465149-1']);
<?php if ($show_secondary_menu == "on") { ?>
	    _gaq.push(['_setCustomVar', 1, 'call_to_action_shown', '<?php echo $cta_version; ?>', 3 ]);
<?php } ?>
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
	</script>
<?php } ?>

</head>
<?php
$classes = '';
if ($show_secondary_menu == "on") {
	$classes = 'secondary_menu';
}
?>
<body <?php body_class($classes); ?>>

<?php
if (!empty($cassiopee_header)) {
    $cassiopee_header->showHeader("fr");
}
?>

<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/bkg.jpg" id="bg" alt="">
<div id="curtain"></div>
<div class="wrapper">
<div class="language-switcher">
<?php echo qtrans_generateLanguageSelectCode('text'); ?>
</div>
<header>
<div class="logo-container">
<a href="<?php echo qtrans_convertURL(home_url('/'));?>"><img id="logo" src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/logo.png" alt="Logo"/></a>
</div>

<div class="right-container">
<div class="right-upper-top"></div>

<nav class="main-nav"><?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?></nav>

<div class="right-upper-bottom"></div>
</div><!-- right-container-->

</header>
