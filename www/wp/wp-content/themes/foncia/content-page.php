<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

function foncia_show_cta_1() {
	$cta_title       = get_post_meta(HOME_PAGE, "cta_title_" .       qtrans_getLanguage(), true);
	$cta_text        = get_post_meta(HOME_PAGE, "cta_text_" .        qtrans_getLanguage(), true);
	$cta_button_text = get_post_meta(HOME_PAGE, "cta_button_text_" . qtrans_getLanguage(), true);

	if (!empty($cta_title) and !empty($cta_text) and !empty($cta_button_text)) {
		?><div class="call_to_action_container">
			<div class="call_to_action call_to_action1">
				<div class="call_to_action_text call_to_action1_text">
					<div class="call_to_action_title"><?php echo $cta_title; ?></div>
					<?php echo $cta_text; ?>
				</div><!-- .call_to_action_text -->
				<a class="call_to_action_button" href="<?php echo get_permalink( CONTACT_PAGE ); ?>"><?php echo $cta_button_text; ?></a>
			</div>
		</div><?php
	}
}

function foncia_show_cta_2() {
	$cta_text        = get_post_meta(HOME_PAGE, "cta_2_text_" .        qtrans_getLanguage(), true);
	$cta_button_text = get_post_meta(HOME_PAGE, "cta_2_button_text_" . qtrans_getLanguage(), true);

	if (!empty($cta_text) and !empty($cta_button_text)) {
		?><div class="call_to_action_container">
			<div class="call_to_action call_to_action2">
				<a class="call_to_action_button" href="<?php echo get_permalink( PLANS_PAGE ); ?>#apartment-list"><?php echo $cta_button_text; ?></a>
				<div class="call_to_action_text call_to_action2_text">
					<?php echo $cta_text; ?>
				</div><!-- .call_to_action_text -->
			</div>
		</div><?php
	}
}

?>


<article class="content-wrapper" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<section class="column-left">
<div class="page-title-container"><h2 class="page-title"><?php the_title(); ?></h2></div>

		<?php the_content(); ?>

</section>

<section class="column-right">
<?php

$label_img = get_post_meta($post->ID, "label_image", true);
$label_text = get_post_meta($post->ID, "label_text_" . qtrans_getLanguage(), true);
$single_img = get_post_meta($post->ID, "single_image", true);

$txt_content_fr = get_post_meta($post->ID, "text_content", true);
$txt_content_de = get_post_meta($post->ID, "text_content_de", true);
$txt_content_en = get_post_meta($post->ID, "text_content_en", true);


$show_secondary_menu = get_post_meta($post->ID, "show_secondary_menu", true);

$latitude = get_post_meta($post->ID, "latitude", true);
$longitude = get_post_meta($post->ID, "longitude", true);

if ($show_secondary_menu == "on") {
	global $cta_version;
	if ($cta_version == 1) {
		foncia_show_cta_1();
	}
	else {
		foncia_show_cta_2();
	}
	wp_nav_menu( array('theme_location' => 'secondary' ) );
}
elseif($label_img) { ?>
	<div class="col-img-container">

	<div class="right-headline-container"><h3 class="right-headline"><span class="gradient">
	<?php echo $label_text; ?>
	</span></h3></div>

	<?php echo wp_get_attachment_image($label_img, 'full'); ?>
	</div>
<?php }

if($single_img) {
	echo wp_get_attachment_image($single_img, 'full');
}

if($txt_content_fr && qtrans_getLanguage() == 'fr') {
	echo $txt_content_fr;
}

if($txt_content_de && qtrans_getLanguage() == 'de') {
	echo $txt_content_de;
}

if($txt_content_en && qtrans_getLanguage() == 'en') {
	echo $txt_content_en;
}
?>


</section>
<?php if (!empty($latitude) and !empty($longitude)) { ?>
	<script>
	var latitude = <?php echo $latitude; ?>,
		longitude = <?php echo $longitude; ?>;
	</script>
	<div class="map-wrapper">
		<div id="map">&nbsp;</div>
<?php } ?>
	<div>
		<?php edit_post_link( __( 'Edit', 'twentyeleven' ), '<span class="edit-link">', '</span>' ); ?>
	</div><!-- .entry-meta -->
<br clear="both" />
</article><!-- end content-wrapper--><!-- #post-<?php the_ID(); ?> -->
