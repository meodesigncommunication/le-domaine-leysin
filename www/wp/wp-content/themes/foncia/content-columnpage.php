<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>


<article class="content-wrapper" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<section class="one-column">
<div class="page-title-container"><h2 class="page-title"><?php the_title(); ?></h2></div>

		<?php the_content(); ?>
		
</section>



	<div>
		<?php edit_post_link( __( 'Edit', 'twentyeleven' ), '<span class="edit-link">', '</span>' ); ?>
	</div><!-- .entry-meta -->
<br clear="both" />	
</article><!-- end content-wrapper--><!-- #post-<?php the_ID(); ?> -->
