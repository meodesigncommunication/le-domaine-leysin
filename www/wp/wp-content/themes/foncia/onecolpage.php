<?php
/*
Template Name: one column page
*/

get_header(); ?>
<?php $on = get_post_meta($post->ID, "slider_on", true); ?>
<?php if($on) : ?>

<div class="slideshow-container">
<?php
	$fieldset_data = get_cft_fieldset_data('slider_' . qtrans_getLanguage() . '-');
	$fieldset_data = order_array_num($fieldset_data, 'slider_' . qtrans_getLanguage() . '-banner_order', 'ASC');
	if($fieldset_data) :
?>

<?php echo '<ul class="slideshow">'; ?>
<?php foreach($fieldset_data as $k => $item) : if(!$item['slider_' . qtrans_getLanguage() . '-banner_image']) continue; ?>
<?php $slider_img = $item['slider_' . qtrans_getLanguage() . '-banner_image']; ?>

<?php echo '<li>'; ?>
<div class="item">
		<?php 
		echo wp_get_attachment_image($slider_img, 'full');
		?>	
</div>
<?php echo '</li>'; ?>
<?php endforeach; ?>
<?php echo '</ul>'; ?>

<?php endif; ?>
</div><!-- end slider-container-->
<?php endif; ?>
		

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'columnpage' ); ?>
					
				<?php endwhile; // end of the loop. ?>
	
			

<?php get_footer(); ?>