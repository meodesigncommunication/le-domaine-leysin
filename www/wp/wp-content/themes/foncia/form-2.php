<?php
/**
 * Template Name: Form 2
 *
 * @package ledomainleysin
 */

?>

<html>
	<head>
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); echo '?' . filemtime( get_stylesheet_directory() . '/style.css'); ?>" />
	</head>
	<body>
		<div id="pdf_download_pages_2">
			<div id="pdf_download" class="pdf_popup">
				<form id="customer_registration_form_2" action="<?php echo get_permalink( get_page_by_path( 'form-handler' ) ); ?>" method="post">
					<div class="popup_heading"><?php _e('To receive your download link,<br>please provide the following information:', LEYSIN_TEXT_DOMAIN); ?></div>
					<input                   type="text" class="surname"    name="surname"    placeholder="<?php _e('Surname', LEYSIN_TEXT_DOMAIN); ?>*">
					<input                   type="text" class="first_name" name="first_name" placeholder="<?php _e('First name', LEYSIN_TEXT_DOMAIN); ?>*">
					<input                   type="text" class="email"      name="email"      placeholder="<?php _e('email', LEYSIN_TEXT_DOMAIN); ?>*">
					<input                   type="text" class="phone"      name="phone"      placeholder="<?php _e('Tel', LEYSIN_TEXT_DOMAIN); ?>*">
					<input                   type="text" class="address"    name="address"    placeholder="<?php _e('Address', LEYSIN_TEXT_DOMAIN); ?>*">
					<input id="pdf_postcode" type="text" class="postcode"   name="postcode"   placeholder="<?php _e('Postcode', LEYSIN_TEXT_DOMAIN); ?>*">
					<input id="pdf_city"     type="text" class="city"       name="city"       placeholder="<?php _e('City', LEYSIN_TEXT_DOMAIN); ?>*">
					<?php echo foncia_get_country_select(); ?>
					<div class="caption"> * <?php _e('required fields', LEYSIN_TEXT_DOMAIN); ?></div>
					<button type="submit"><?php _e('send', LEYSIN_TEXT_DOMAIN); ?></button>
					<input id="pdf_apartment_code" type="hidden" name="apartment_code" value="">
					<input type="hidden"                    name="language"   value="<?php echo qtrans_getLanguage(); ?>">
				</form>
			</div>
			<div style="display:none" id="pdf_download_thanks" class="pdf_popup">
				<div class="popup_heading"><?php _e('The download link for your PDF has been sent to your email address', LEYSIN_TEXT_DOMAIN); ?></div>
				<div class="pdf_email_address"></div>
				<button type="submit"><?php _e('OK', LEYSIN_TEXT_DOMAIN); ?></button>
			</div>
			<div style="display:none" id="pdf_confirm_email" class="pdf_popup">
				<form id="pdf_download_form" action="<?php echo get_permalink( get_page_by_path( 'form-handler' ) ); ?>" method="post">
					<div class="popup_heading"><?php _e('The download link for your PDF will be sent to your email address', LEYSIN_TEXT_DOMAIN); ?></div>
					<input class="email" type="hidden" name="email" value="">
					<input class="lot_id"  type="hidden" name="apartment_code" value="">

					<input type="hidden" class="surname"    name="surname"    value="">
					<input type="hidden" class="first_name" name="first_name" value="">
					<input type="hidden" class="phone"      name="phone"      value="">
					<input type="hidden" class="address"    name="address"    value="">
					<input type="hidden" class="postcode"   name="postcode"   value="">
					<input type="hidden" class="city"       name="city"       value="">
					<input type="hidden"                    name="language"   value="<?php echo qtrans_getLanguage(); ?>">

					<div class="pdf_email_address"></div>
					<button class="change"><?php _e('Change address', LEYSIN_TEXT_DOMAIN); ?></button>
					<button type="submit"><?php _e('OK', LEYSIN_TEXT_DOMAIN); ?></button>
				</form>
			</div>
		</div>
	</body>
</html>