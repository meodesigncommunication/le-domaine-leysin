<?php
	$sold_label = foncia_get_sold_label();
	$available_label = foncia_get_available_label();
	$appartment_classes = array(
		1 => 'appartment-left',
		2 => 'appartment-center',
		3 => 'appartment-right',
		4 => 'appartment-behind'
	);


	$area_labels = array(
		'apartment' => __('Apartment area', LEYSIN_TEXT_DOMAIN),
		'terrace'   => __('Terrace area', LEYSIN_TEXT_DOMAIN),
		'balcony'   => __('Balcony area', LEYSIN_TEXT_DOMAIN),
		'garden'   => __('Garden area', LEYSIN_TEXT_DOMAIN),
		'ponderee'  => __('Surface area', LEYSIN_TEXT_DOMAIN)
	);

	$pdf_download_button_content = __("Download apartment<br>details as a PDF", LEYSIN_TEXT_DOMAIN);

	$buildings = foncia_get_building_meta();
?>
<div class="floor-tab tab">
	<div id="floor"><?php
		$building_number = 1;
		foreach ($buildings as $building) {
			if (!$building['floor_data']) {
				continue;
			}
			$apt = 1;
			$building_name = $building['chalet_type'];
			$floor_number = 1;
			foreach($building['floor_data'] as $item) {
				if(!$item['floor-floor_name']) {
					continue;
				}
				$floor_img = $item['floor-image'];
				$floor_name = $item['floor-floor_name'];
				$floor_style = $item['floor-floor_style'];
				?>
				<div class="floor" id="floor_<?php echo $building_number; ?>_<?php echo $floor_number; ?>" data-building-name="Chalet <?php echo $building_name; ?>" data-floor-name="<?php echo $floor_name; ?>">
					<?php

					$availability = array();
					$floor_appartment = array();
					foreach( array_keys($appartment_classes) as $index ) {
						if(!$item['floor-code' . $index]) {
							continue;
						}
						$availability[$index] = $item['floor-vendu' . $index];
						$apartment_code[$index] = $item['floor-code' . $index];

						$floor_details = '<ul>';
						$floor_details .= '<li><b>' . __('Apartment', LEYSIN_TEXT_DOMAIN) . ' ' . $item['floor-code' . $index] . '</b></li>';
						foreach ($area_labels as $field => $label) {
							if ($item['floor-area-' . $field . $index ]) {
								$floor_details .= '<li>' . $label . ' : ' . $item['floor-area-' . $field . $index ] . 'm<sup>2</sup></li>';
							}
						}
						$floor_details .= '</ul>';

						$floor_appartment[$index] = $floor_details;
					}
					?>
					<div class="flplan-img-container type-<?php echo $building_name; ?> style-<?php echo $floor_style; ?>">
						<div class="flplan-img">
							<div class="layer-over green floorNum<?php echo $apt; ?>">
								<div class="layerHover"></div>
								<?php foreach( $appartment_classes as $index => $class ) {
									if (!is_null($availability[$index])) {?>

									<div class="appartment <?php echo $class; ?> <?php echo $availability[$index]; ?>" data-code="<?php echo $apartment_code[$index]; ?>">
										<a class="fancybox-form" href="#pdf_download"><div class="button"><?php echo $pdf_download_button_content; ?></div></a>
									</div>
								<?php } } ?>

								<?php foreach( $appartment_classes as $index => $class ) { ?>
									<?php if($availability[$index] == 'vendu') { ?>
										<div class="layer-over red<?php echo $index; ?>">
											<a href="#">&nbsp;</a>
											<div class="label-sold<?php echo $index; ?>">
												<?php echo $sold_label; ?>
											</div>
										</div>
									<?php } ?>
								<?php } ?>
							</div><!-- end floorNum#-->
							<?php echo wp_get_attachment_image($floor_img, 'full'); ?>
						</div>
					</div><!-- end flplan-img-container-->
					<?php if ($building_number == 1 &&  $floor_number >= 3 && count($floor_appartment) == 4) {
						foreach (array(1,4,2,3) as $index) {
							?><div class="four-appartment appartment<?php echo $index; ?>-position"><?php echo $floor_appartment[$index]; ?></div><?php
						}
					}
					else if ($building_number == 1 && count($floor_appartment) == 2) {
						foreach (array(2,3) as $index) {
							?><div class="two-appartment appartment<?php echo $index; ?>-position"><?php echo $floor_appartment[$index]; ?></div><?php
						}
					} else if($building_number == 1 && $floor_number == 4) {
						foreach (array(1,2,4) as $index) {
							?><div class="three-appartment appartment<?php echo $index; ?>-position"><?php echo 							$floor_appartment[$index]; ?></div><?php
						}
					} else {
						foreach( $floor_appartment as $index => $value ) {
							if ($index == 4) { ?>
								<div class="appartment<?php echo $index; ?>-position"><?php echo $value; ?></div>
							<?php }
						}

						foreach( $floor_appartment as $index => $value ) {
							if ($index != 4) { ?>
								<div class="appartment<?php echo $index; ?>-position"><?php echo $value; ?></div>
							<?php }
						}
					} ?>
				</div>
				<?php $apt++;
				$floor_number++;
			}
			$building_number++;
		} ?>
		<div class="clear"></div>
	</div>
</div>
