<?php

$post_ids = array(
	2, // Projet
	5, // Situation
	8, // Appartements
	12, // Plans
	14, // Lex Weber
	16 // Contact
);
?>

	<footer>
<nav class="footer-nav">
<ul>
<?php
	foreach ($post_ids as $index => $post_id) {
		$url = get_permalink( $post_id );
		$title = $post_id == 2 ? "Leysin" : get_the_title($post_id);
		$trailer = ($index == count($post_ids) - 1) ? '' : '&ndash;';
		?><li><a href="<?php echo $url;?>"><?php echo $title; ?></a><?php echo $trailer; ?></li><?php
	}
?>
</ul>
</nav>

<div class="footer-contacts-container">
<p>PIERRE ETOILE SA | Chemin du Flonzel 59 - 1093 La Conversion<br />
+41 (0) 21 799 9000 | +41 (0) 79 108 8958 | <a href="mailto:lmanzoni@pierreetoile.ch">lmanzoni@pierreetoile.ch</a> </p>

<div class="footer-social-links addthis_toolbox addthis_default_style "
        addthis:url="<?php echo home_url('/');?>"
        addthis:title="<?php bloginfo( 'name' ); ?>"
        addthis:description="<?php bloginfo( 'description' ); ?>">
<a class="addthis_button_facebook"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/fb_ico.png" alt="Facebook" /></a>
<a class="addthis_button_email"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/mail_ico.png" alt="email" /></a>
        </div>
</div>
<br style="clear: both;" />
</footer>
<div class="copyright">design and development by <a href="http://www.allmeo.com/">meo</a> | all rights reserved &copy;</div>
</div><!-- end wrapper-->

<?php wp_footer(); ?>
<script type="text/javascript">
//Track Google
//Contact form handling
jQuery('.cform.contact .sendbutton').click(function (e) {
	e.preventDefault();
	if (typeof _gaq !== "undefined") {
		_gaq.push(['_trackEvent', 'Contact form FR', 'Submitted']);
	}
});
jQuery('.cform.contact-en .sendbutton').click(function (e) {
	e.preventDefault();
	if (typeof _gaq !== "undefined") {
		_gaq.push(['_trackEvent', 'Contact form EN', 'Submitted']);
	}
});
jQuery('.cform.contact-de .sendbutton').click(function (e) {
	e.preventDefault();
	if (typeof _gaq !== "undefined") {
		_gaq.push(['_trackEvent', 'Contact form DE', 'Submitted']);
	}
});
</script>
</body>
</html>