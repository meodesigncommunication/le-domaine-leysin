<?php

/*
Plugin Name: Cassiopee header
Version: 1.0
Author: MEO
Author URI: http://www.meomeo.ch/
*/
/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@allmeo.com
*/


$plugin_root = plugin_dir_path( __FILE__ );

require_once( $plugin_root . 'classes/class-cassiopee-header.php');

global $cassiopee_header;

$cassiopee_header = new CassiopeeHeader();
