(function($){

	$(function() {

		get_offset = function() {
			var offset = $('.cassiopee-header-large-content').outerHeight();
			return - offset;
		};

		$('.cassiopee-header').waypoint('sticky', {
			direction: 'down',
			stuckClass: 'stuck',
			offset: get_offset,
			handler: function(e) {
				$('.cassiopee-header').css({
					top: '0px'
				});  // Ensure top if 0 if we're not currently "sticky"
				$('.cassiopee-header.stuck').css({
					top: get_offset() + 'px'
				});
			}
		});

		$('.cassiopee-header a').click(function (e) {
			var link = $(this).attr('href');
			if (typeof ga !== "undefined") {
				// Universal Google Analytics
				ga('send', 'pageview', {
					'page': link
				});
			}
			if (typeof _gaq !== "undefined") {
				// Legacy Google Analytics
				_gaq.push(['_trackPageview', link]);
			}
		});

		$(window).on('resize', function() {
			$.waypoints('refresh');
		}).trigger('resize');

	});

})(jQuery);
