<?php

if(!class_exists('WP_List_Table')) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class CS_Table extends WP_List_Table
{
    public static
        $name    = 'contact_storage',
        $columns = array(
            'id'         => 'ID',
            'first_name' => 'First name',
            'last_name'  => 'Last name',
            'address'    => 'Address',
            'email'      => 'Email',
            'phone'      => 'Phone',
            'language'   => 'Language',
            'message'    => 'Message',
            'created_at' => 'Created at',
        	/* Patch */
            'postal_code'=> 'Postal Code',
            'city'   	 => 'City',
            'country'	 => 'Country',
        	/*  */
        )
    ;

    public $itemsPerPage = 20;

    function __construct( $args = array() ) {
        parent::__construct(array(
            'singular' => 'contact',
            'plural'   => 'contacts',
            'ajax'     => false
        ));
    }

    public static function setup() {
        /** @var $wpdb wpdb */
        global $wpdb;

        $table_name = $wpdb->prefix . CS_Table::$name;

        $wpdb->query("
            CREATE TABLE IF NOT EXISTS `{$table_name}` (
              `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `first_name` varchar(250) NOT NULL,
              `last_name` varchar(250) NOT NULL,
              `address` varchar(250) NOT NULL,
              `email` varchar(250) NOT NULL,
              `phone` varchar(250) NOT NULL,
              `language` varchar(250) NOT NULL,
              `message` text NOT NULL,
              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `postal_code` varchar(250) NOT NULL,
              `city` varchar(250) NOT NULL,
              `country` varchar(250) NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        ); /* Patch adding postal_code, city and country fields */
    }

    function prepare_items() {
        /** @var $wpdb wpdb */
        global $wpdb;

        $this->_column_headers = array($this->get_columns(), array(), array());

        $this->items = $wpdb->get_results(sprintf(
            "SELECT * FROM `%s` LIMIT %d, %d",
            self::wpName(), $this->itemsPerPage * ($this->get_pagenum() - 1), $this->itemsPerPage
        ), ARRAY_A);

        $total_items = $wpdb->get_var(sprintf("SELECT COUNT(*) FROM `%s`", self::wpName()));
        $total_pages = ceil($total_items / $this->itemsPerPage);

        $this->set_pagination_args(array(
            'total_items' => $total_items,
            'total_pages' => $total_pages,
            'per_page'    => $this->itemsPerPage
        ));
    }

    function get_columns() {
        $columns = self::$columns;
        unset($columns['id']);

        return $columns;
    }

    function column_default($item, $column_name){
        return $item[$column_name];
    }

    public static function insert($data) {
        /** @var $wpdb wpdb */
        global $wpdb;

        $wpdb->insert(self::wpName(), $data);
    }

    public static function wpName() {
        /** @var $wpdb wpdb */
        global $wpdb;

        return $wpdb->prefix . self::$name;
    }
}
