<?php
/**
 * @var $pages array
 * @var $selectedPages array
 */
?>
<div class="wrap">
    <div id="icon-options-general" class="icon32">
        <br/>
    </div>
    <h2>Settings</h2>

    <form method="post" action="">
        <table class="form-table">
            <tbody>
            <tr valign="top">
                <th scope="row">
                    <label>API URL</label>
                </th>
                <td>
                    <input name="<?php echo CS_API_OPTION_NAME ?>" type="text" class="regular-text" value="<?php echo get_option(CS_API_OPTION_NAME) ?>"/>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">
                    <label>Form Pages</label>
                </th>
                <td>
                    <select name="<?php echo CS_PAGES_OPTION_NAME ?>[]" size="10" multiple>
                        <?php foreach($pages as $page): ?>
                            <option value="<?php echo $page->ID ?>" <?php echo in_array($page->ID, $selectedPages) ? 'selected':'' ?>><?php _e($page->post_title) ?></option>
                        <?php endforeach ?>
                    </select>
                </td>
            </tr>
            </tbody>
        </table>
        <p class="submit">
            <input type="submit" class="button button-primary" value="Save Changes"/>
        </p>
    </form>
</div>
